import './App.css';
import Games from './components/games/Games';

function App() {
  return (
    <div className="App">
      <Games />
    </div>
  );
}

export default App;
