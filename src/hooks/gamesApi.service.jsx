import Axios from "axios";
import testApi from "./testApi";

const useGamesApi = () => {
  /*const options = {
    method: "GET",
    url: "https://rawg-video-games-database.p.rapidapi.com/games/873d82e72b7b40d6a5334382e829048a",
    headers: {
      "X-RapidAPI-Key": "521507e677msh94e6925dcb2d2ddp15d335jsn9778a70aa207",
      "X-RapidAPI-Host": "rawg-video-games-database.p.rapidapi.com",
    },
  };*/

  const testFetchGames = () => {
    return mapListGames(testApi.results);
  };

  const url = "https://api.rawg.io/api/games?key=";
  const key = "873d82e72b7b40d6a5334382e829048aAh";
  const paramPageSize = "&page_size=10";

  const fetchGames = async () => {
    try {
      const resp = await Axios.get(url + key + paramPageSize);
      const games = mapListGames(resp.data.results);
      console.log(games);
    } catch (err) {
      // Handle Error Here
      console.error(err);
    }
  };

  const mapListGames = (games) => {
    const newList = [];
    games.forEach((game) => {
      const { background_image, name, genres, id, metacritic } = game;
      const newgame = { background_image, name, genres, id, metacritic };
      newList.push(newgame);
    });
    return newList;
  };

  return {
    fetchGames,
    testFetchGames,
  };
};

export default useGamesApi;
