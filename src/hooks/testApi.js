const testApi = {
  "count": 802376,
  "next": "https://api.rawg.io/api/games?key=873d82e72b7b40d6a5334382e829048a&page=2&page_size=10",
  "previous": null,
  "results": [
    {
      "id": 3498,
      "slug": "grand-theft-auto-v",
      "name": "Grand Theft Auto V",
      "released": "2013-09-17",
      "tba": false,
      "background_image": "https://media.rawg.io/media/games/456/456dea5e1c7e3cd07060c14e96612001.jpg",
      "rating": 4.47,
      "rating_top": 5,
      "ratings": [
        {
          "id": 5,
          "title": "exceptional",
          "count": 3529,
          "percent": 59.0
        },
        {
          "id": 4,
          "title": "recommended",
          "count": 1959,
          "percent": 32.75
        },
        {
          "id": 3,
          "title": "meh",
          "count": 385,
          "percent": 6.44
        },
        {
          "id": 1,
          "title": "skip",
          "count": 108,
          "percent": 1.81
        }
      ],
      "ratings_count": 5906,
      "reviews_text_count": 43,
      "added": 18034,
      "added_by_status": {
        "yet": 456,
        "owned": 10521,
        "beaten": 4962,
        "toplay": 527,
        "dropped": 913,
        "playing": 655
      },
      "metacritic": 91,
      "playtime": 72,
      "suggestions_count": 414,
      "updated": "2022-09-28T06:24:05",
      "user_game": null,
      "reviews_count": 5981,
      "saturated_color": "0f0f0f",
      "dominant_color": "0f0f0f",
      "platforms": [
        {
          "platform": {
            "id": 4,
            "name": "PC",
            "slug": "pc",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 483690,
            "image_background": "https://media.rawg.io/media/games/618/618c2031a07bbff6b4f611f10b6bcdbc.jpg"
          },
          "released_at": "2013-09-17",
          "requirements_en": {
            "minimum": "Minimum:OS: Windows 10 64 Bit, Windows 8.1 64 Bit, Windows 8 64 Bit, Windows 7 64 Bit Service Pack 1, Windows Vista 64 Bit Service Pack 2* (*NVIDIA video card recommended if running Vista OS)Processor: Intel Core 2 Quad CPU Q6600 @ 2.40GHz (4 CPUs) / AMD Phenom 9850 Quad-Core Processor (4 CPUs) @ 2.5GHzMemory: 4 GB RAMGraphics: NVIDIA 9800 GT 1GB / AMD HD 4870 1GB (DX 10, 10.1, 11)Storage: 72 GB available spaceSound Card: 100% DirectX 10 compatibleAdditional Notes: Over time downloadable content and programming changes will change the system requirements for this game.  Please refer to your hardware manufacturer and www.rockstargames.com/support for current compatibility information. Some system components such as mobile chipsets, integrated, and AGP graphics cards may be incompatible. Unlisted specifications may not be supported by publisher.     Other requirements:  Installation and online play requires log-in to Rockstar Games Social Club (13+) network; internet connection required for activation, online play, and periodic entitlement verification; software installations required including Rockstar Games Social Club platform, DirectX , Chromium, and Microsoft Visual C++ 2008 sp1 Redistributable Package, and authentication software that recognizes certain hardware attributes for entitlement, digital rights management, system, and other support purposes.     SINGLE USE SERIAL CODE REGISTRATION VIA INTERNET REQUIRED; REGISTRATION IS LIMITED TO ONE ROCKSTAR GAMES SOCIAL CLUB ACCOUNT (13+) PER SERIAL CODE; ONLY ONE PC LOG-IN ALLOWED PER SOCIAL CLUB ACCOUNT AT ANY TIME; SERIAL CODE(S) ARE NON-TRANSFERABLE ONCE USED; SOCIAL CLUB ACCOUNTS ARE NON-TRANSFERABLE.  Partner Requirements:  Please check the terms of service of this site before purchasing this software.",
            "recommended": "Recommended:OS: Windows 10 64 Bit, Windows 8.1 64 Bit, Windows 8 64 Bit, Windows 7 64 Bit Service Pack 1Processor: Intel Core i5 3470 @ 3.2GHz (4 CPUs) / AMD X8 FX-8350 @ 4GHz (8 CPUs)Memory: 8 GB RAMGraphics: NVIDIA GTX 660 2GB / AMD HD 7870 2GBStorage: 72 GB available spaceSound Card: 100% DirectX 10 compatibleAdditional Notes:"
          },
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 186,
            "name": "Xbox Series S/X",
            "slug": "xbox-series-x",
            "image": null,
            "year_end": null,
            "year_start": 2020,
            "games_count": 619,
            "image_background": "https://media.rawg.io/media/games/253/2534a46f3da7fa7c315f1387515ca393.jpg"
          },
          "released_at": "2013-09-17",
          "requirements_en": null,
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 18,
            "name": "PlayStation 4",
            "slug": "playstation4",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 6479,
            "image_background": "https://media.rawg.io/media/games/fc1/fc1307a2774506b5bd65d7e8424664a7.jpg"
          },
          "released_at": "2013-09-17",
          "requirements_en": null,
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 16,
            "name": "PlayStation 3",
            "slug": "playstation3",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 3582,
            "image_background": "https://media.rawg.io/media/games/562/562553814dd54e001a541e4ee83a591c.jpg"
          },
          "released_at": "2013-09-17",
          "requirements_en": null,
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 14,
            "name": "Xbox 360",
            "slug": "xbox360",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 2777,
            "image_background": "https://media.rawg.io/media/games/15c/15c95a4915f88a3e89c821526afe05fc.jpg"
          },
          "released_at": "2013-09-17",
          "requirements_en": null,
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 1,
            "name": "Xbox One",
            "slug": "xbox-one",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 5399,
            "image_background": "https://media.rawg.io/media/games/490/49016e06ae2103881ff6373248843069.jpg"
          },
          "released_at": "2013-09-17",
          "requirements_en": null,
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 187,
            "name": "PlayStation 5",
            "slug": "playstation5",
            "image": null,
            "year_end": null,
            "year_start": 2020,
            "games_count": 691,
            "image_background": "https://media.rawg.io/media/games/4be/4be6a6ad0364751a96229c56bf69be59.jpg"
          },
          "released_at": "2013-09-17",
          "requirements_en": null,
          "requirements_ru": null
        }
      ],
      "parent_platforms": [
        {
          "platform": {
            "id": 1,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 2,
            "name": "PlayStation",
            "slug": "playstation"
          }
        },
        {
          "platform": {
            "id": 3,
            "name": "Xbox",
            "slug": "xbox"
          }
        }
      ],
      "genres": [
        {
          "id": 4,
          "name": "Action",
          "slug": "action",
          "games_count": 162171,
          "image_background": "https://media.rawg.io/media/games/511/5118aff5091cb3efec399c808f8c598f.jpg"
        },
        {
          "id": 3,
          "name": "Adventure",
          "slug": "adventure",
          "games_count": 123284,
          "image_background": "https://media.rawg.io/media/games/b54/b54598d1d5cc31899f4f0a7e3122a7b0.jpg"
        }
      ],
      "stores": [
        {
          "id": 290375,
          "store": {
            "id": 3,
            "name": "PlayStation Store",
            "slug": "playstation-store",
            "domain": "store.playstation.com",
            "games_count": 7797,
            "image_background": "https://media.rawg.io/media/games/f87/f87457e8347484033cb34cde6101d08d.jpg"
          }
        },
        {
          "id": 438095,
          "store": {
            "id": 11,
            "name": "Epic Games",
            "slug": "epic-games",
            "domain": "epicgames.com",
            "games_count": 1091,
            "image_background": "https://media.rawg.io/media/games/951/951572a3dd1e42544bd39a5d5b42d234.jpg"
          }
        },
        {
          "id": 290376,
          "store": {
            "id": 1,
            "name": "Steam",
            "slug": "steam",
            "domain": "store.steampowered.com",
            "games_count": 67190,
            "image_background": "https://media.rawg.io/media/games/c4b/c4b0cab189e73432de3a250d8cf1c84e.jpg"
          }
        },
        {
          "id": 290377,
          "store": {
            "id": 7,
            "name": "Xbox 360 Store",
            "slug": "xbox360",
            "domain": "marketplace.xbox.com",
            "games_count": 1915,
            "image_background": "https://media.rawg.io/media/games/120/1201a40e4364557b124392ee50317b99.jpg"
          }
        },
        {
          "id": 290378,
          "store": {
            "id": 2,
            "name": "Xbox Store",
            "slug": "xbox-store",
            "domain": "microsoft.com",
            "games_count": 4752,
            "image_background": "https://media.rawg.io/media/games/4cf/4cfc6b7f1850590a4634b08bfab308ab.jpg"
          }
        }
      ],
      "clip": null,
      "tags": [
        {
          "id": 31,
          "name": "Singleplayer",
          "slug": "singleplayer",
          "language": "eng",
          "games_count": 183363,
          "image_background": "https://media.rawg.io/media/games/588/588c6bdff3d4baf66ec36b1c05b793bf.jpg"
        },
        {
          "id": 40847,
          "name": "Steam Achievements",
          "slug": "steam-achievements",
          "language": "eng",
          "games_count": 27271,
          "image_background": "https://media.rawg.io/media/games/b45/b45575f34285f2c4479c9a5f719d972e.jpg"
        },
        {
          "id": 7,
          "name": "Multiplayer",
          "slug": "multiplayer",
          "language": "eng",
          "games_count": 32955,
          "image_background": "https://media.rawg.io/media/games/328/3283617cb7d75d67257fc58339188742.jpg"
        },
        {
          "id": 13,
          "name": "Atmospheric",
          "slug": "atmospheric",
          "language": "eng",
          "games_count": 24853,
          "image_background": "https://media.rawg.io/media/games/b7d/b7d3f1715fa8381a4e780173a197a615.jpg"
        },
        {
          "id": 40836,
          "name": "Full controller support",
          "slug": "full-controller-support",
          "language": "eng",
          "games_count": 12760,
          "image_background": "https://media.rawg.io/media/games/b49/b4912b5dbfc7ed8927b65f05b8507f6c.jpg"
        },
        {
          "id": 42,
          "name": "Great Soundtrack",
          "slug": "great-soundtrack",
          "language": "eng",
          "games_count": 3212,
          "image_background": "https://media.rawg.io/media/games/c4b/c4b0cab189e73432de3a250d8cf1c84e.jpg"
        },
        {
          "id": 24,
          "name": "RPG",
          "slug": "rpg",
          "language": "eng",
          "games_count": 15378,
          "image_background": "https://media.rawg.io/media/games/c6b/c6bfece1daf8d06bc0a60632ac78e5bf.jpg"
        },
        {
          "id": 18,
          "name": "Co-op",
          "slug": "co-op",
          "language": "eng",
          "games_count": 8915,
          "image_background": "https://media.rawg.io/media/games/73e/73eecb8909e0c39fb246f457b5d6cbbe.jpg"
        },
        {
          "id": 36,
          "name": "Open World",
          "slug": "open-world",
          "language": "eng",
          "games_count": 5529,
          "image_background": "https://media.rawg.io/media/games/d82/d82990b9c67ba0d2d09d4e6fa88885a7.jpg"
        },
        {
          "id": 411,
          "name": "cooperative",
          "slug": "cooperative",
          "language": "eng",
          "games_count": 3604,
          "image_background": "https://media.rawg.io/media/games/baf/baf9905270314e07e6850cffdb51df41.jpg"
        },
        {
          "id": 8,
          "name": "First-Person",
          "slug": "first-person",
          "language": "eng",
          "games_count": 24488,
          "image_background": "https://media.rawg.io/media/games/511/5118aff5091cb3efec399c808f8c598f.jpg"
        },
        {
          "id": 149,
          "name": "Third Person",
          "slug": "third-person",
          "language": "eng",
          "games_count": 7938,
          "image_background": "https://media.rawg.io/media/games/5c0/5c0dd63002cb23f804aab327d40ef119.jpg"
        },
        {
          "id": 4,
          "name": "Funny",
          "slug": "funny",
          "language": "eng",
          "games_count": 20437,
          "image_background": "https://media.rawg.io/media/games/10d/10d19e52e5e8415d16a4d344fe711874.jpg"
        },
        {
          "id": 37,
          "name": "Sandbox",
          "slug": "sandbox",
          "language": "eng",
          "games_count": 5293,
          "image_background": "https://media.rawg.io/media/games/48c/48cb04ca483be865e3a83119c94e6097.jpg"
        },
        {
          "id": 123,
          "name": "Comedy",
          "slug": "comedy",
          "language": "eng",
          "games_count": 9740,
          "image_background": "https://media.rawg.io/media/games/c89/c89ca70716080733d03724277df2c6c7.jpg"
        },
        {
          "id": 150,
          "name": "Third-Person Shooter",
          "slug": "third-person-shooter",
          "language": "eng",
          "games_count": 2561,
          "image_background": "https://media.rawg.io/media/games/e3d/e3ddc524c6292a435d01d97cc5f42ea7.jpg"
        },
        {
          "id": 62,
          "name": "Moddable",
          "slug": "moddable",
          "language": "eng",
          "games_count": 723,
          "image_background": "https://media.rawg.io/media/games/c22/c22d804ac753c72f2617b3708a625dec.jpg"
        },
        {
          "id": 144,
          "name": "Crime",
          "slug": "crime",
          "language": "eng",
          "games_count": 2367,
          "image_background": "https://media.rawg.io/media/games/456/456dea5e1c7e3cd07060c14e96612001.jpg"
        },
        {
          "id": 62349,
          "name": "vr mod",
          "slug": "vr-mod",
          "language": "eng",
          "games_count": 17,
          "image_background": "https://media.rawg.io/media/screenshots/1bb/1bb3f78f0fe43b5d5ca2f3da5b638840.jpg"
        }
      ],
      "esrb_rating": {
        "id": 4,
        "name": "Mature",
        "slug": "mature"
      },
      "short_screenshots": [
        {
          "id": -1,
          "image": "https://media.rawg.io/media/games/456/456dea5e1c7e3cd07060c14e96612001.jpg"
        },
        {
          "id": 1827221,
          "image": "https://media.rawg.io/media/screenshots/a7c/a7c43871a54bed6573a6a429451564ef.jpg"
        },
        {
          "id": 1827222,
          "image": "https://media.rawg.io/media/screenshots/cf4/cf4367daf6a1e33684bf19adb02d16d6.jpg"
        },
        {
          "id": 1827223,
          "image": "https://media.rawg.io/media/screenshots/f95/f9518b1d99210c0cae21fc09e95b4e31.jpg"
        },
        {
          "id": 1827225,
          "image": "https://media.rawg.io/media/screenshots/a5c/a5c95ea539c87d5f538763e16e18fb99.jpg"
        },
        {
          "id": 1827226,
          "image": "https://media.rawg.io/media/screenshots/a7e/a7e990bc574f4d34e03b5926361d1ee7.jpg"
        },
        {
          "id": 1827227,
          "image": "https://media.rawg.io/media/screenshots/592/592e2501d8734b802b2a34fee2df59fa.jpg"
        }
      ]
    },
    {
      "id": 3328,
      "slug": "the-witcher-3-wild-hunt",
      "name": "The Witcher 3: Wild Hunt",
      "released": "2015-05-18",
      "tba": false,
      "background_image": "https://media.rawg.io/media/games/618/618c2031a07bbff6b4f611f10b6bcdbc.jpg",
      "rating": 4.67,
      "rating_top": 5,
      "ratings": [
        {
          "id": 5,
          "title": "exceptional",
          "count": 4379,
          "percent": 78.03
        },
        {
          "id": 4,
          "title": "recommended",
          "count": 872,
          "percent": 15.54
        },
        {
          "id": 3,
          "title": "meh",
          "count": 226,
          "percent": 4.03
        },
        {
          "id": 1,
          "title": "skip",
          "count": 135,
          "percent": 2.41
        }
      ],
      "ratings_count": 5534,
      "reviews_text_count": 56,
      "added": 16869,
      "added_by_status": {
        "yet": 954,
        "owned": 9731,
        "beaten": 3986,
        "toplay": 668,
        "dropped": 745,
        "playing": 785
      },
      "metacritic": 92,
      "playtime": 46,
      "suggestions_count": 662,
      "updated": "2022-09-28T06:12:05",
      "user_game": null,
      "reviews_count": 5612,
      "saturated_color": "0f0f0f",
      "dominant_color": "0f0f0f",
      "platforms": [
        {
          "platform": {
            "id": 7,
            "name": "Nintendo Switch",
            "slug": "nintendo-switch",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 4992,
            "image_background": "https://media.rawg.io/media/games/713/713269608dc8f2f40f5a670a14b2de94.jpg"
          },
          "released_at": "2015-05-18",
          "requirements_en": null,
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 187,
            "name": "PlayStation 5",
            "slug": "playstation5",
            "image": null,
            "year_end": null,
            "year_start": 2020,
            "games_count": 691,
            "image_background": "https://media.rawg.io/media/games/4be/4be6a6ad0364751a96229c56bf69be59.jpg"
          },
          "released_at": "2015-05-18",
          "requirements_en": null,
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 186,
            "name": "Xbox Series S/X",
            "slug": "xbox-series-x",
            "image": null,
            "year_end": null,
            "year_start": 2020,
            "games_count": 619,
            "image_background": "https://media.rawg.io/media/games/253/2534a46f3da7fa7c315f1387515ca393.jpg"
          },
          "released_at": "2015-05-18",
          "requirements_en": null,
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 1,
            "name": "Xbox One",
            "slug": "xbox-one",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 5399,
            "image_background": "https://media.rawg.io/media/games/490/49016e06ae2103881ff6373248843069.jpg"
          },
          "released_at": "2015-05-18",
          "requirements_en": null,
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 4,
            "name": "PC",
            "slug": "pc",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 483690,
            "image_background": "https://media.rawg.io/media/games/618/618c2031a07bbff6b4f611f10b6bcdbc.jpg"
          },
          "released_at": "2015-05-18",
          "requirements_en": null,
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 18,
            "name": "PlayStation 4",
            "slug": "playstation4",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 6479,
            "image_background": "https://media.rawg.io/media/games/fc1/fc1307a2774506b5bd65d7e8424664a7.jpg"
          },
          "released_at": "2015-05-18",
          "requirements_en": null,
          "requirements_ru": null
        }
      ],
      "parent_platforms": [
        {
          "platform": {
            "id": 1,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 2,
            "name": "PlayStation",
            "slug": "playstation"
          }
        },
        {
          "platform": {
            "id": 3,
            "name": "Xbox",
            "slug": "xbox"
          }
        },
        {
          "platform": {
            "id": 7,
            "name": "Nintendo",
            "slug": "nintendo"
          }
        }
      ],
      "genres": [
        {
          "id": 4,
          "name": "Action",
          "slug": "action",
          "games_count": 162171,
          "image_background": "https://media.rawg.io/media/games/511/5118aff5091cb3efec399c808f8c598f.jpg"
        },
        {
          "id": 3,
          "name": "Adventure",
          "slug": "adventure",
          "games_count": 123284,
          "image_background": "https://media.rawg.io/media/games/b54/b54598d1d5cc31899f4f0a7e3122a7b0.jpg"
        },
        {
          "id": 5,
          "name": "RPG",
          "slug": "role-playing-games-rpg",
          "games_count": 49440,
          "image_background": "https://media.rawg.io/media/games/7cf/7cfc9220b401b7a300e409e539c9afd5.jpg"
        }
      ],
      "stores": [
        {
          "id": 354780,
          "store": {
            "id": 5,
            "name": "GOG",
            "slug": "gog",
            "domain": "gog.com",
            "games_count": 4412,
            "image_background": "https://media.rawg.io/media/games/562/562553814dd54e001a541e4ee83a591c.jpg"
          }
        },
        {
          "id": 3565,
          "store": {
            "id": 3,
            "name": "PlayStation Store",
            "slug": "playstation-store",
            "domain": "store.playstation.com",
            "games_count": 7797,
            "image_background": "https://media.rawg.io/media/games/f87/f87457e8347484033cb34cde6101d08d.jpg"
          }
        },
        {
          "id": 305376,
          "store": {
            "id": 1,
            "name": "Steam",
            "slug": "steam",
            "domain": "store.steampowered.com",
            "games_count": 67190,
            "image_background": "https://media.rawg.io/media/games/c4b/c4b0cab189e73432de3a250d8cf1c84e.jpg"
          }
        },
        {
          "id": 312313,
          "store": {
            "id": 2,
            "name": "Xbox Store",
            "slug": "xbox-store",
            "domain": "microsoft.com",
            "games_count": 4752,
            "image_background": "https://media.rawg.io/media/games/4cf/4cfc6b7f1850590a4634b08bfab308ab.jpg"
          }
        },
        {
          "id": 676022,
          "store": {
            "id": 6,
            "name": "Nintendo Store",
            "slug": "nintendo",
            "domain": "nintendo.com",
            "games_count": 8857,
            "image_background": "https://media.rawg.io/media/games/879/879c930f9c6787c920153fa2df452eb3.jpg"
          }
        }
      ],
      "clip": null,
      "tags": [
        {
          "id": 31,
          "name": "Singleplayer",
          "slug": "singleplayer",
          "language": "eng",
          "games_count": 183363,
          "image_background": "https://media.rawg.io/media/games/588/588c6bdff3d4baf66ec36b1c05b793bf.jpg"
        },
        {
          "id": 13,
          "name": "Atmospheric",
          "slug": "atmospheric",
          "language": "eng",
          "games_count": 24853,
          "image_background": "https://media.rawg.io/media/games/b7d/b7d3f1715fa8381a4e780173a197a615.jpg"
        },
        {
          "id": 40836,
          "name": "Full controller support",
          "slug": "full-controller-support",
          "language": "eng",
          "games_count": 12760,
          "image_background": "https://media.rawg.io/media/games/b49/b4912b5dbfc7ed8927b65f05b8507f6c.jpg"
        },
        {
          "id": 42,
          "name": "Great Soundtrack",
          "slug": "great-soundtrack",
          "language": "eng",
          "games_count": 3212,
          "image_background": "https://media.rawg.io/media/games/c4b/c4b0cab189e73432de3a250d8cf1c84e.jpg"
        },
        {
          "id": 24,
          "name": "RPG",
          "slug": "rpg",
          "language": "eng",
          "games_count": 15378,
          "image_background": "https://media.rawg.io/media/games/c6b/c6bfece1daf8d06bc0a60632ac78e5bf.jpg"
        },
        {
          "id": 118,
          "name": "Story Rich",
          "slug": "story-rich",
          "language": "eng",
          "games_count": 16047,
          "image_background": "https://media.rawg.io/media/games/960/960b601d9541cec776c5fa42a00bf6c4.jpg"
        },
        {
          "id": 36,
          "name": "Open World",
          "slug": "open-world",
          "language": "eng",
          "games_count": 5529,
          "image_background": "https://media.rawg.io/media/games/d82/d82990b9c67ba0d2d09d4e6fa88885a7.jpg"
        },
        {
          "id": 149,
          "name": "Third Person",
          "slug": "third-person",
          "language": "eng",
          "games_count": 7938,
          "image_background": "https://media.rawg.io/media/games/5c0/5c0dd63002cb23f804aab327d40ef119.jpg"
        },
        {
          "id": 64,
          "name": "Fantasy",
          "slug": "fantasy",
          "language": "eng",
          "games_count": 21547,
          "image_background": "https://media.rawg.io/media/games/4e6/4e6e8e7f50c237d76f38f3c885dae3d2.jpg"
        },
        {
          "id": 37,
          "name": "Sandbox",
          "slug": "sandbox",
          "language": "eng",
          "games_count": 5293,
          "image_background": "https://media.rawg.io/media/games/48c/48cb04ca483be865e3a83119c94e6097.jpg"
        },
        {
          "id": 97,
          "name": "Action RPG",
          "slug": "action-rpg",
          "language": "eng",
          "games_count": 5147,
          "image_background": "https://media.rawg.io/media/games/26d/26d4437715bee60138dab4a7c8c59c92.jpg"
        },
        {
          "id": 41,
          "name": "Dark",
          "slug": "dark",
          "language": "eng",
          "games_count": 12166,
          "image_background": "https://media.rawg.io/media/games/d5a/d5a24f9f71315427fa6e966fdd98dfa6.jpg"
        },
        {
          "id": 44,
          "name": "Nudity",
          "slug": "nudity",
          "language": "eng",
          "games_count": 4077,
          "image_background": "https://media.rawg.io/media/games/8ca/8ca40b562a755d6a0e30d48e6c74b178.jpg"
        },
        {
          "id": 336,
          "name": "controller support",
          "slug": "controller-support",
          "language": "eng",
          "games_count": 293,
          "image_background": "https://media.rawg.io/media/games/b17/b17485d757ca36b5f1ad376b6b096885.jpg"
        },
        {
          "id": 145,
          "name": "Choices Matter",
          "slug": "choices-matter",
          "language": "eng",
          "games_count": 2628,
          "image_background": "https://media.rawg.io/media/games/6cd/6cd653e0aaef5ff8bbd295bf4bcb12eb.jpg"
        },
        {
          "id": 192,
          "name": "Mature",
          "slug": "mature",
          "language": "eng",
          "games_count": 1632,
          "image_background": "https://media.rawg.io/media/games/960/960b601d9541cec776c5fa42a00bf6c4.jpg"
        },
        {
          "id": 40,
          "name": "Dark Fantasy",
          "slug": "dark-fantasy",
          "language": "eng",
          "games_count": 2724,
          "image_background": "https://media.rawg.io/media/games/ee3/ee3e10193aafc3230ba1cae426967d10.jpg"
        },
        {
          "id": 66,
          "name": "Medieval",
          "slug": "medieval",
          "language": "eng",
          "games_count": 4589,
          "image_background": "https://media.rawg.io/media/games/c22/c22d804ac753c72f2617b3708a625dec.jpg"
        },
        {
          "id": 82,
          "name": "Magic",
          "slug": "magic",
          "language": "eng",
          "games_count": 7230,
          "image_background": "https://media.rawg.io/media/games/639/639ce7d7fecbb9f0717e9fbc1180f8f8.jpg"
        },
        {
          "id": 218,
          "name": "Multiple Endings",
          "slug": "multiple-endings",
          "language": "eng",
          "games_count": 5891,
          "image_background": "https://media.rawg.io/media/games/618/618c2031a07bbff6b4f611f10b6bcdbc.jpg"
        }
      ],
      "esrb_rating": {
        "id": 4,
        "name": "Mature",
        "slug": "mature"
      },
      "short_screenshots": [
        {
          "id": -1,
          "image": "https://media.rawg.io/media/games/618/618c2031a07bbff6b4f611f10b6bcdbc.jpg"
        },
        {
          "id": 30336,
          "image": "https://media.rawg.io/media/screenshots/1ac/1ac19f31974314855ad7be266adeb500.jpg"
        },
        {
          "id": 30337,
          "image": "https://media.rawg.io/media/screenshots/6a0/6a08afca95261a2fe221ea9e01d28762.jpg"
        },
        {
          "id": 30338,
          "image": "https://media.rawg.io/media/screenshots/cdd/cdd31b6b4a687425a87b5ce231ac89d7.jpg"
        },
        {
          "id": 30339,
          "image": "https://media.rawg.io/media/screenshots/862/862397b153221a625922d3bb66337834.jpg"
        },
        {
          "id": 30340,
          "image": "https://media.rawg.io/media/screenshots/166/166787c442a45f52f4f230c33fd7d605.jpg"
        },
        {
          "id": 30342,
          "image": "https://media.rawg.io/media/screenshots/f63/f6373ee614046d81503d63f167181803.jpg"
        }
      ]
    },
    {
      "id": 4200,
      "slug": "portal-2",
      "name": "Portal 2",
      "released": "2011-04-18",
      "tba": false,
      "background_image": "https://media.rawg.io/media/games/328/3283617cb7d75d67257fc58339188742.jpg",
      "rating": 4.61,
      "rating_top": 5,
      "ratings": [
        {
          "id": 5,
          "title": "exceptional",
          "count": 3457,
          "percent": 70.18
        },
        {
          "id": 4,
          "title": "recommended",
          "count": 1230,
          "percent": 24.97
        },
        {
          "id": 3,
          "title": "meh",
          "count": 139,
          "percent": 2.82
        },
        {
          "id": 1,
          "title": "skip",
          "count": 100,
          "percent": 2.03
        }
      ],
      "ratings_count": 4884,
      "reviews_text_count": 29,
      "added": 15863,
      "added_by_status": {
        "yet": 528,
        "owned": 9759,
        "beaten": 4670,
        "toplay": 305,
        "dropped": 476,
        "playing": 125
      },
      "metacritic": 95,
      "playtime": 11,
      "suggestions_count": 541,
      "updated": "2022-09-28T06:17:49",
      "user_game": null,
      "reviews_count": 4926,
      "saturated_color": "0f0f0f",
      "dominant_color": "0f0f0f",
      "platforms": [
        {
          "platform": {
            "id": 14,
            "name": "Xbox 360",
            "slug": "xbox360",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 2777,
            "image_background": "https://media.rawg.io/media/games/15c/15c95a4915f88a3e89c821526afe05fc.jpg"
          },
          "released_at": "2011-04-19",
          "requirements_en": null,
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 6,
            "name": "Linux",
            "slug": "linux",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 72202,
            "image_background": "https://media.rawg.io/media/games/9dd/9ddabb34840ea9227556670606cf8ea3.jpg"
          },
          "released_at": "2011-04-19",
          "requirements_en": null,
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 5,
            "name": "macOS",
            "slug": "macos",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 98332,
            "image_background": "https://media.rawg.io/media/games/b54/b54598d1d5cc31899f4f0a7e3122a7b0.jpg"
          },
          "released_at": null,
          "requirements_en": null,
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 16,
            "name": "PlayStation 3",
            "slug": "playstation3",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 3582,
            "image_background": "https://media.rawg.io/media/games/562/562553814dd54e001a541e4ee83a591c.jpg"
          },
          "released_at": "2011-04-19",
          "requirements_en": null,
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 4,
            "name": "PC",
            "slug": "pc",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 483690,
            "image_background": "https://media.rawg.io/media/games/618/618c2031a07bbff6b4f611f10b6bcdbc.jpg"
          },
          "released_at": null,
          "requirements_en": null,
          "requirements_ru": {
            "minimum": "Core 2 Duo/Athlon X2 2 ГГц,1 Гб памяти,GeForce 7600/Radeon X800,10 Гб на винчестере,интернет-соединение",
            "recommended": "Core 2 Duo/Athlon X2 2.5 ГГц,2 Гб памяти,GeForce GTX 280/Radeon HD 2600,10 Гб на винчестере,интернет-соединение"
          }
        },
        {
          "platform": {
            "id": 1,
            "name": "Xbox One",
            "slug": "xbox-one",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 5399,
            "image_background": "https://media.rawg.io/media/games/490/49016e06ae2103881ff6373248843069.jpg"
          },
          "released_at": "2011-04-18",
          "requirements_en": null,
          "requirements_ru": null
        }
      ],
      "parent_platforms": [
        {
          "platform": {
            "id": 1,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 2,
            "name": "PlayStation",
            "slug": "playstation"
          }
        },
        {
          "platform": {
            "id": 3,
            "name": "Xbox",
            "slug": "xbox"
          }
        },
        {
          "platform": {
            "id": 5,
            "name": "Apple Macintosh",
            "slug": "mac"
          }
        },
        {
          "platform": {
            "id": 6,
            "name": "Linux",
            "slug": "linux"
          }
        }
      ],
      "genres": [
        {
          "id": 2,
          "name": "Shooter",
          "slug": "shooter",
          "games_count": 56671,
          "image_background": "https://media.rawg.io/media/games/34b/34b1f1850a1c06fd971bc6ab3ac0ce0e.jpg"
        },
        {
          "id": 7,
          "name": "Puzzle",
          "slug": "puzzle",
          "games_count": 91877,
          "image_background": "https://media.rawg.io/media/games/6fc/6fcb1c529c764700d55f3bbc1b0fbb5b.jpg"
        }
      ],
      "stores": [
        {
          "id": 465889,
          "store": {
            "id": 2,
            "name": "Xbox Store",
            "slug": "xbox-store",
            "domain": "microsoft.com",
            "games_count": 4752,
            "image_background": "https://media.rawg.io/media/games/4cf/4cfc6b7f1850590a4634b08bfab308ab.jpg"
          }
        },
        {
          "id": 13134,
          "store": {
            "id": 1,
            "name": "Steam",
            "slug": "steam",
            "domain": "store.steampowered.com",
            "games_count": 67190,
            "image_background": "https://media.rawg.io/media/games/c4b/c4b0cab189e73432de3a250d8cf1c84e.jpg"
          }
        },
        {
          "id": 4526,
          "store": {
            "id": 3,
            "name": "PlayStation Store",
            "slug": "playstation-store",
            "domain": "store.playstation.com",
            "games_count": 7797,
            "image_background": "https://media.rawg.io/media/games/f87/f87457e8347484033cb34cde6101d08d.jpg"
          }
        },
        {
          "id": 33916,
          "store": {
            "id": 7,
            "name": "Xbox 360 Store",
            "slug": "xbox360",
            "domain": "marketplace.xbox.com",
            "games_count": 1915,
            "image_background": "https://media.rawg.io/media/games/120/1201a40e4364557b124392ee50317b99.jpg"
          }
        }
      ],
      "clip": null,
      "tags": [
        {
          "id": 31,
          "name": "Singleplayer",
          "slug": "singleplayer",
          "language": "eng",
          "games_count": 183363,
          "image_background": "https://media.rawg.io/media/games/588/588c6bdff3d4baf66ec36b1c05b793bf.jpg"
        },
        {
          "id": 40847,
          "name": "Steam Achievements",
          "slug": "steam-achievements",
          "language": "eng",
          "games_count": 27271,
          "image_background": "https://media.rawg.io/media/games/b45/b45575f34285f2c4479c9a5f719d972e.jpg"
        },
        {
          "id": 7,
          "name": "Multiplayer",
          "slug": "multiplayer",
          "language": "eng",
          "games_count": 32955,
          "image_background": "https://media.rawg.io/media/games/328/3283617cb7d75d67257fc58339188742.jpg"
        },
        {
          "id": 13,
          "name": "Atmospheric",
          "slug": "atmospheric",
          "language": "eng",
          "games_count": 24853,
          "image_background": "https://media.rawg.io/media/games/b7d/b7d3f1715fa8381a4e780173a197a615.jpg"
        },
        {
          "id": 40836,
          "name": "Full controller support",
          "slug": "full-controller-support",
          "language": "eng",
          "games_count": 12760,
          "image_background": "https://media.rawg.io/media/games/b49/b4912b5dbfc7ed8927b65f05b8507f6c.jpg"
        },
        {
          "id": 40849,
          "name": "Steam Cloud",
          "slug": "steam-cloud",
          "language": "eng",
          "games_count": 12450,
          "image_background": "https://media.rawg.io/media/games/310/3106b0e012271c5ffb16497b070be739.jpg"
        },
        {
          "id": 7808,
          "name": "steam-trading-cards",
          "slug": "steam-trading-cards",
          "language": "eng",
          "games_count": 7582,
          "image_background": "https://media.rawg.io/media/games/46d/46d98e6910fbc0706e2948a7cc9b10c5.jpg"
        },
        {
          "id": 18,
          "name": "Co-op",
          "slug": "co-op",
          "language": "eng",
          "games_count": 8915,
          "image_background": "https://media.rawg.io/media/games/73e/73eecb8909e0c39fb246f457b5d6cbbe.jpg"
        },
        {
          "id": 118,
          "name": "Story Rich",
          "slug": "story-rich",
          "language": "eng",
          "games_count": 16047,
          "image_background": "https://media.rawg.io/media/games/960/960b601d9541cec776c5fa42a00bf6c4.jpg"
        },
        {
          "id": 411,
          "name": "cooperative",
          "slug": "cooperative",
          "language": "eng",
          "games_count": 3604,
          "image_background": "https://media.rawg.io/media/games/baf/baf9905270314e07e6850cffdb51df41.jpg"
        },
        {
          "id": 8,
          "name": "First-Person",
          "slug": "first-person",
          "language": "eng",
          "games_count": 24488,
          "image_background": "https://media.rawg.io/media/games/511/5118aff5091cb3efec399c808f8c598f.jpg"
        },
        {
          "id": 32,
          "name": "Sci-fi",
          "slug": "sci-fi",
          "language": "eng",
          "games_count": 15625,
          "image_background": "https://media.rawg.io/media/games/b72/b7233d5d5b1e75e86bb860ccc7aeca85.jpg"
        },
        {
          "id": 30,
          "name": "FPS",
          "slug": "fps",
          "language": "eng",
          "games_count": 11305,
          "image_background": "https://media.rawg.io/media/games/fc1/fc1307a2774506b5bd65d7e8424664a7.jpg"
        },
        {
          "id": 9,
          "name": "Online Co-Op",
          "slug": "online-co-op",
          "language": "eng",
          "games_count": 3795,
          "image_background": "https://media.rawg.io/media/games/34b/34b1f1850a1c06fd971bc6ab3ac0ce0e.jpg"
        },
        {
          "id": 4,
          "name": "Funny",
          "slug": "funny",
          "language": "eng",
          "games_count": 20437,
          "image_background": "https://media.rawg.io/media/games/10d/10d19e52e5e8415d16a4d344fe711874.jpg"
        },
        {
          "id": 189,
          "name": "Female Protagonist",
          "slug": "female-protagonist",
          "language": "eng",
          "games_count": 9211,
          "image_background": "https://media.rawg.io/media/games/62c/62c7c8b28a27b83680b22fb9d33fc619.jpg"
        },
        {
          "id": 123,
          "name": "Comedy",
          "slug": "comedy",
          "language": "eng",
          "games_count": 9740,
          "image_background": "https://media.rawg.io/media/games/c89/c89ca70716080733d03724277df2c6c7.jpg"
        },
        {
          "id": 75,
          "name": "Local Co-Op",
          "slug": "local-co-op",
          "language": "eng",
          "games_count": 4669,
          "image_background": "https://media.rawg.io/media/games/c6b/c6bfece1daf8d06bc0a60632ac78e5bf.jpg"
        },
        {
          "id": 11669,
          "name": "stats",
          "slug": "stats",
          "language": "eng",
          "games_count": 4254,
          "image_background": "https://media.rawg.io/media/games/f52/f5206d55f918edf8ee07803101106fa6.jpg"
        },
        {
          "id": 40852,
          "name": "Steam Workshop",
          "slug": "steam-workshop",
          "language": "eng",
          "games_count": 1228,
          "image_background": "https://media.rawg.io/media/games/0fd/0fd84d36596a83ef2e5a35f63a072218.jpg"
        },
        {
          "id": 25,
          "name": "Space",
          "slug": "space",
          "language": "eng",
          "games_count": 39290,
          "image_background": "https://media.rawg.io/media/games/f87/f87457e8347484033cb34cde6101d08d.jpg"
        },
        {
          "id": 40838,
          "name": "Includes level editor",
          "slug": "includes-level-editor",
          "language": "eng",
          "games_count": 1541,
          "image_background": "https://media.rawg.io/media/games/12e/12ea6b35b65df38258e25885a0a392a6.jpg"
        },
        {
          "id": 40833,
          "name": "Captions available",
          "slug": "captions-available",
          "language": "eng",
          "games_count": 1196,
          "image_background": "https://media.rawg.io/media/games/33b/33b825c76382931df0fd8ecddf5caebe.jpg"
        },
        {
          "id": 40834,
          "name": "Commentary available",
          "slug": "commentary-available",
          "language": "eng",
          "games_count": 248,
          "image_background": "https://media.rawg.io/media/games/726/7263e11b6bfb15abe35dcfa3b26147f5.jpg"
        },
        {
          "id": 87,
          "name": "Science",
          "slug": "science",
          "language": "eng",
          "games_count": 1392,
          "image_background": "https://media.rawg.io/media/screenshots/0c3/0c383a754af823c31204533b16fdd31a.jpg"
        }
      ],
      "esrb_rating": {
        "id": 2,
        "name": "Everyone 10+",
        "slug": "everyone-10-plus"
      },
      "short_screenshots": [
        {
          "id": -1,
          "image": "https://media.rawg.io/media/games/328/3283617cb7d75d67257fc58339188742.jpg"
        },
        {
          "id": 99018,
          "image": "https://media.rawg.io/media/screenshots/221/221a03c11e5ff9f765d62f60d4b4cbf5.jpg"
        },
        {
          "id": 99019,
          "image": "https://media.rawg.io/media/screenshots/173/1737ff43c14f40294011a209b1012875.jpg"
        },
        {
          "id": 99020,
          "image": "https://media.rawg.io/media/screenshots/b11/b11a2ae0664f0e8a1ef2346f99df26e1.jpg"
        },
        {
          "id": 99021,
          "image": "https://media.rawg.io/media/screenshots/9b1/9b107a790909b31918ebe2f40547cc85.jpg"
        },
        {
          "id": 99022,
          "image": "https://media.rawg.io/media/screenshots/d05/d058fc7f7fa6128916c311eb14267fed.jpg"
        },
        {
          "id": 99023,
          "image": "https://media.rawg.io/media/screenshots/415/41543dcc12dffc8e97d85a56ad42cda8.jpg"
        }
      ]
    },
    {
      "id": 5286,
      "slug": "tomb-raider",
      "name": "Tomb Raider (2013)",
      "released": "2013-03-05",
      "tba": false,
      "background_image": "https://media.rawg.io/media/games/021/021c4e21a1824d2526f925eff6324653.jpg",
      "rating": 4.06,
      "rating_top": 4,
      "ratings": [
        {
          "id": 4,
          "title": "recommended",
          "count": 2091,
          "percent": 60.49
        },
        {
          "id": 5,
          "title": "exceptional",
          "count": 879,
          "percent": 25.43
        },
        {
          "id": 3,
          "title": "meh",
          "count": 388,
          "percent": 11.22
        },
        {
          "id": 1,
          "title": "skip",
          "count": 99,
          "percent": 2.86
        }
      ],
      "ratings_count": 3435,
      "reviews_text_count": 10,
      "added": 14132,
      "added_by_status": {
        "yet": 570,
        "owned": 9184,
        "beaten": 3622,
        "toplay": 218,
        "dropped": 443,
        "playing": 95
      },
      "metacritic": 86,
      "playtime": 10,
      "suggestions_count": 629,
      "updated": "2022-09-28T06:08:35",
      "user_game": null,
      "reviews_count": 3457,
      "saturated_color": "0f0f0f",
      "dominant_color": "0f0f0f",
      "platforms": [
        {
          "platform": {
            "id": 18,
            "name": "PlayStation 4",
            "slug": "playstation4",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 6479,
            "image_background": "https://media.rawg.io/media/games/fc1/fc1307a2774506b5bd65d7e8424664a7.jpg"
          },
          "released_at": "2013-03-05",
          "requirements_en": null,
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 5,
            "name": "macOS",
            "slug": "macos",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 98332,
            "image_background": "https://media.rawg.io/media/games/b54/b54598d1d5cc31899f4f0a7e3122a7b0.jpg"
          },
          "released_at": "2013-03-05",
          "requirements_en": {
            "minimum": "Minimum:\r\n\r\nOS: macOS 10.9.1\r\n\r\nProcessor: 2.0GHz Intel or greater\r\n\r\nMemory: 4GB\r\n\r\nGraphics: 512Mb AMD 4850, 512Mb Nvidia 650M, Intel HD4000\r\n\r\nHard Drive: 14GB"
          },
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 4,
            "name": "PC",
            "slug": "pc",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 483690,
            "image_background": "https://media.rawg.io/media/games/618/618c2031a07bbff6b4f611f10b6bcdbc.jpg"
          },
          "released_at": "2013-03-05",
          "requirements_en": {
            "minimum": "<strong>Minimum:</strong><br><ul class=\"bb_ul\"><li><strong>OS:</strong>Windows XP / Windows Vista / Windows 7<br>\t</li><li><strong>Processor:</strong>1.8 GHz Processor<br>\t</li><li><strong>Memory:</strong>512 MB RAM<br>\t</li><li><strong>Graphics:</strong>3D graphics card compatible with DirectX 9<br>\t</li><li><strong>DirectX®:</strong>9.0<br>\t</li><li><strong>Hard Drive:</strong>2 GB HD space</li></ul>"
          },
          "requirements_ru": {
            "minimum": "i486-100, 8 Мб",
            "recommended": "Pentium 166, 16 Мб"
          }
        },
        {
          "platform": {
            "id": 1,
            "name": "Xbox One",
            "slug": "xbox-one",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 5399,
            "image_background": "https://media.rawg.io/media/games/490/49016e06ae2103881ff6373248843069.jpg"
          },
          "released_at": "2013-03-05",
          "requirements_en": null,
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 14,
            "name": "Xbox 360",
            "slug": "xbox360",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 2777,
            "image_background": "https://media.rawg.io/media/games/15c/15c95a4915f88a3e89c821526afe05fc.jpg"
          },
          "released_at": "2013-03-05",
          "requirements_en": null,
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 16,
            "name": "PlayStation 3",
            "slug": "playstation3",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 3582,
            "image_background": "https://media.rawg.io/media/games/562/562553814dd54e001a541e4ee83a591c.jpg"
          },
          "released_at": "2013-03-05",
          "requirements_en": null,
          "requirements_ru": null
        }
      ],
      "parent_platforms": [
        {
          "platform": {
            "id": 1,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 2,
            "name": "PlayStation",
            "slug": "playstation"
          }
        },
        {
          "platform": {
            "id": 3,
            "name": "Xbox",
            "slug": "xbox"
          }
        },
        {
          "platform": {
            "id": 5,
            "name": "Apple Macintosh",
            "slug": "mac"
          }
        }
      ],
      "genres": [
        {
          "id": 4,
          "name": "Action",
          "slug": "action",
          "games_count": 162171,
          "image_background": "https://media.rawg.io/media/games/511/5118aff5091cb3efec399c808f8c598f.jpg"
        },
        {
          "id": 3,
          "name": "Adventure",
          "slug": "adventure",
          "games_count": 123284,
          "image_background": "https://media.rawg.io/media/games/b54/b54598d1d5cc31899f4f0a7e3122a7b0.jpg"
        }
      ],
      "stores": [
        {
          "id": 33824,
          "store": {
            "id": 7,
            "name": "Xbox 360 Store",
            "slug": "xbox360",
            "domain": "marketplace.xbox.com",
            "games_count": 1915,
            "image_background": "https://media.rawg.io/media/games/120/1201a40e4364557b124392ee50317b99.jpg"
          }
        },
        {
          "id": 13151,
          "store": {
            "id": 1,
            "name": "Steam",
            "slug": "steam",
            "domain": "store.steampowered.com",
            "games_count": 67190,
            "image_background": "https://media.rawg.io/media/games/c4b/c4b0cab189e73432de3a250d8cf1c84e.jpg"
          }
        },
        {
          "id": 5640,
          "store": {
            "id": 3,
            "name": "PlayStation Store",
            "slug": "playstation-store",
            "domain": "store.playstation.com",
            "games_count": 7797,
            "image_background": "https://media.rawg.io/media/games/f87/f87457e8347484033cb34cde6101d08d.jpg"
          }
        },
        {
          "id": 218233,
          "store": {
            "id": 8,
            "name": "Google Play",
            "slug": "google-play",
            "domain": "play.google.com",
            "games_count": 16868,
            "image_background": "https://media.rawg.io/media/games/8d6/8d69eb6c32ed6acfd75f82d532144993.jpg"
          }
        },
        {
          "id": 79036,
          "store": {
            "id": 4,
            "name": "App Store",
            "slug": "apple-appstore",
            "domain": "apps.apple.com",
            "games_count": 73767,
            "image_background": "https://media.rawg.io/media/games/713/713269608dc8f2f40f5a670a14b2de94.jpg"
          }
        },
        {
          "id": 713685,
          "store": {
            "id": 11,
            "name": "Epic Games",
            "slug": "epic-games",
            "domain": "epicgames.com",
            "games_count": 1091,
            "image_background": "https://media.rawg.io/media/games/951/951572a3dd1e42544bd39a5d5b42d234.jpg"
          }
        }
      ],
      "clip": null,
      "tags": [
        {
          "id": 31,
          "name": "Singleplayer",
          "slug": "singleplayer",
          "language": "eng",
          "games_count": 183363,
          "image_background": "https://media.rawg.io/media/games/588/588c6bdff3d4baf66ec36b1c05b793bf.jpg"
        },
        {
          "id": 7,
          "name": "Multiplayer",
          "slug": "multiplayer",
          "language": "eng",
          "games_count": 32955,
          "image_background": "https://media.rawg.io/media/games/328/3283617cb7d75d67257fc58339188742.jpg"
        },
        {
          "id": 13,
          "name": "Atmospheric",
          "slug": "atmospheric",
          "language": "eng",
          "games_count": 24853,
          "image_background": "https://media.rawg.io/media/games/b7d/b7d3f1715fa8381a4e780173a197a615.jpg"
        },
        {
          "id": 40836,
          "name": "Full controller support",
          "slug": "full-controller-support",
          "language": "eng",
          "games_count": 12760,
          "image_background": "https://media.rawg.io/media/games/b49/b4912b5dbfc7ed8927b65f05b8507f6c.jpg"
        },
        {
          "id": 24,
          "name": "RPG",
          "slug": "rpg",
          "language": "eng",
          "games_count": 15378,
          "image_background": "https://media.rawg.io/media/games/c6b/c6bfece1daf8d06bc0a60632ac78e5bf.jpg"
        },
        {
          "id": 149,
          "name": "Third Person",
          "slug": "third-person",
          "language": "eng",
          "games_count": 7938,
          "image_background": "https://media.rawg.io/media/games/5c0/5c0dd63002cb23f804aab327d40ef119.jpg"
        },
        {
          "id": 193,
          "name": "Classic",
          "slug": "classic",
          "language": "eng",
          "games_count": 1711,
          "image_background": "https://media.rawg.io/media/games/5fa/5fae5fec3c943179e09da67a4427d68f.jpg"
        },
        {
          "id": 6,
          "name": "Exploration",
          "slug": "exploration",
          "language": "eng",
          "games_count": 16934,
          "image_background": "https://media.rawg.io/media/games/214/214b29aeff13a0ae6a70fc4426e85991.jpg"
        },
        {
          "id": 189,
          "name": "Female Protagonist",
          "slug": "female-protagonist",
          "language": "eng",
          "games_count": 9211,
          "image_background": "https://media.rawg.io/media/games/62c/62c7c8b28a27b83680b22fb9d33fc619.jpg"
        },
        {
          "id": 15,
          "name": "Stealth",
          "slug": "stealth",
          "language": "eng",
          "games_count": 5245,
          "image_background": "https://media.rawg.io/media/games/310/3106b0e012271c5ffb16497b070be739.jpg"
        },
        {
          "id": 150,
          "name": "Third-Person Shooter",
          "slug": "third-person-shooter",
          "language": "eng",
          "games_count": 2561,
          "image_background": "https://media.rawg.io/media/games/e3d/e3ddc524c6292a435d01d97cc5f42ea7.jpg"
        },
        {
          "id": 74,
          "name": "Retro",
          "slug": "retro",
          "language": "eng",
          "games_count": 35247,
          "image_background": "https://media.rawg.io/media/games/9a1/9a18c226cf379272c698f26d2b79b3da.jpg"
        },
        {
          "id": 69,
          "name": "Action-Adventure",
          "slug": "action-adventure",
          "language": "eng",
          "games_count": 12045,
          "image_background": "https://media.rawg.io/media/games/b72/b7233d5d5b1e75e86bb860ccc7aeca85.jpg"
        },
        {
          "id": 110,
          "name": "Cinematic",
          "slug": "cinematic",
          "language": "eng",
          "games_count": 1056,
          "image_background": "https://media.rawg.io/media/games/07a/07a74470a2618fd71945db0619602baf.jpg"
        },
        {
          "id": 269,
          "name": "Quick-Time Events",
          "slug": "quick-time-events",
          "language": "eng",
          "games_count": 313,
          "image_background": "https://media.rawg.io/media/screenshots/59e/59e83c7c485f9915205dd23ced6c0b12.jpg"
        },
        {
          "id": 126,
          "name": "Dinosaurs",
          "slug": "dinosaurs",
          "language": "eng",
          "games_count": 1525,
          "image_background": "https://media.rawg.io/media/games/c14/c1484e3baaa7a2c048d457c8a55980ef.jpg"
        },
        {
          "id": 306,
          "name": "Lara Croft",
          "slug": "lara-croft",
          "language": "eng",
          "games_count": 14,
          "image_background": "https://media.rawg.io/media/games/e6b/e6b025951f9a72673f41c0588fb85758.jpg"
        }
      ],
      "esrb_rating": {
        "id": 4,
        "name": "Mature",
        "slug": "mature"
      },
      "short_screenshots": [
        {
          "id": -1,
          "image": "https://media.rawg.io/media/games/021/021c4e21a1824d2526f925eff6324653.jpg"
        },
        {
          "id": 99160,
          "image": "https://media.rawg.io/media/screenshots/4f9/4f9d5efdecfb63cb99f1baa4c0ceb3bf.jpg"
        },
        {
          "id": 99161,
          "image": "https://media.rawg.io/media/screenshots/80f/80f373082b2a74da3f9c3fe2b877dcd0.jpg"
        },
        {
          "id": 99162,
          "image": "https://media.rawg.io/media/screenshots/a87/a8733e877f8fbe45e4a727c22a06aa2e.jpg"
        },
        {
          "id": 99163,
          "image": "https://media.rawg.io/media/screenshots/3f9/3f91678c6805a76419fa4ea3a045a909.jpg"
        },
        {
          "id": 99164,
          "image": "https://media.rawg.io/media/screenshots/417/4170bf07be43a8d8249193883f87f1c1.jpg"
        },
        {
          "id": 99165,
          "image": "https://media.rawg.io/media/screenshots/2a4/2a4250f83ad9e959d8b4ca9376ae34ea.jpg"
        }
      ]
    },
    {
      "id": 4291,
      "slug": "counter-strike-global-offensive",
      "name": "Counter-Strike: Global Offensive",
      "released": "2012-08-21",
      "tba": false,
      "background_image": "https://media.rawg.io/media/games/736/73619bd336c894d6941d926bfd563946.jpg",
      "rating": 3.57,
      "rating_top": 4,
      "ratings": [
        {
          "id": 4,
          "title": "recommended",
          "count": 1411,
          "percent": 46.94
        },
        {
          "id": 3,
          "title": "meh",
          "count": 790,
          "percent": 26.28
        },
        {
          "id": 5,
          "title": "exceptional",
          "count": 477,
          "percent": 15.87
        },
        {
          "id": 1,
          "title": "skip",
          "count": 328,
          "percent": 10.91
        }
      ],
      "ratings_count": 2980,
      "reviews_text_count": 20,
      "added": 13712,
      "added_by_status": {
        "yet": 217,
        "owned": 10410,
        "beaten": 825,
        "toplay": 64,
        "dropped": 1629,
        "playing": 567
      },
      "metacritic": 81,
      "playtime": 65,
      "suggestions_count": 581,
      "updated": "2022-09-28T06:14:27",
      "user_game": null,
      "reviews_count": 3006,
      "saturated_color": "0f0f0f",
      "dominant_color": "0f0f0f",
      "platforms": [
        {
          "platform": {
            "id": 4,
            "name": "PC",
            "slug": "pc",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 483690,
            "image_background": "https://media.rawg.io/media/games/618/618c2031a07bbff6b4f611f10b6bcdbc.jpg"
          },
          "released_at": "2012-08-21",
          "requirements_en": {
            "minimum": "<strong>Minimum:</strong><br><ul class=\"bb_ul\"><li><strong>OS:</strong> Windows® 7/Vista/XP<br></li><li><strong>Processor:</strong> Intel® Core™ 2 Duo E6600 or AMD Phenom™ X3 8750 processor or better<br></li><li><strong>Memory:</strong> 2 GB RAM<br></li><li><strong>Graphics:</strong> Video card must be 256 MB or more and should be a DirectX 9-compatible with support for Pixel Shader 3.0<br></li><li><strong>DirectX:</strong> Version 9.0c<br></li><li><strong>Storage:</strong> 15 GB available space</li></ul>"
          },
          "requirements_ru": {
            "minimum": "Сore 2 Duo/Athlon x2 64 1.8 ГГц,2 Гб памяти,GeForce 8800/Radeon X9800,7.6 Гб на винчестере,интернет-соединение",
            "recommended": "Core 2 Duo E6600/Phenom X3 8750,4 Гб памяти,GeForce 480 GTX/Radeon HD 6970,7.6 Гб на винчестере,интернет-соединение"
          }
        },
        {
          "platform": {
            "id": 14,
            "name": "Xbox 360",
            "slug": "xbox360",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 2777,
            "image_background": "https://media.rawg.io/media/games/15c/15c95a4915f88a3e89c821526afe05fc.jpg"
          },
          "released_at": "2012-08-21",
          "requirements_en": null,
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 16,
            "name": "PlayStation 3",
            "slug": "playstation3",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 3582,
            "image_background": "https://media.rawg.io/media/games/562/562553814dd54e001a541e4ee83a591c.jpg"
          },
          "released_at": "2012-08-21",
          "requirements_en": null,
          "requirements_ru": null
        }
      ],
      "parent_platforms": [
        {
          "platform": {
            "id": 1,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 2,
            "name": "PlayStation",
            "slug": "playstation"
          }
        },
        {
          "platform": {
            "id": 3,
            "name": "Xbox",
            "slug": "xbox"
          }
        }
      ],
      "genres": [
        {
          "id": 4,
          "name": "Action",
          "slug": "action",
          "games_count": 162171,
          "image_background": "https://media.rawg.io/media/games/511/5118aff5091cb3efec399c808f8c598f.jpg"
        },
        {
          "id": 2,
          "name": "Shooter",
          "slug": "shooter",
          "games_count": 56671,
          "image_background": "https://media.rawg.io/media/games/34b/34b1f1850a1c06fd971bc6ab3ac0ce0e.jpg"
        }
      ],
      "stores": [
        {
          "id": 4619,
          "store": {
            "id": 3,
            "name": "PlayStation Store",
            "slug": "playstation-store",
            "domain": "store.playstation.com",
            "games_count": 7797,
            "image_background": "https://media.rawg.io/media/games/f87/f87457e8347484033cb34cde6101d08d.jpg"
          }
        },
        {
          "id": 11489,
          "store": {
            "id": 1,
            "name": "Steam",
            "slug": "steam",
            "domain": "store.steampowered.com",
            "games_count": 67190,
            "image_background": "https://media.rawg.io/media/games/c4b/c4b0cab189e73432de3a250d8cf1c84e.jpg"
          }
        },
        {
          "id": 49169,
          "store": {
            "id": 7,
            "name": "Xbox 360 Store",
            "slug": "xbox360",
            "domain": "marketplace.xbox.com",
            "games_count": 1915,
            "image_background": "https://media.rawg.io/media/games/120/1201a40e4364557b124392ee50317b99.jpg"
          }
        }
      ],
      "clip": null,
      "tags": [
        {
          "id": 40847,
          "name": "Steam Achievements",
          "slug": "steam-achievements",
          "language": "eng",
          "games_count": 27271,
          "image_background": "https://media.rawg.io/media/games/b45/b45575f34285f2c4479c9a5f719d972e.jpg"
        },
        {
          "id": 7,
          "name": "Multiplayer",
          "slug": "multiplayer",
          "language": "eng",
          "games_count": 32955,
          "image_background": "https://media.rawg.io/media/games/328/3283617cb7d75d67257fc58339188742.jpg"
        },
        {
          "id": 40836,
          "name": "Full controller support",
          "slug": "full-controller-support",
          "language": "eng",
          "games_count": 12760,
          "image_background": "https://media.rawg.io/media/games/b49/b4912b5dbfc7ed8927b65f05b8507f6c.jpg"
        },
        {
          "id": 7808,
          "name": "steam-trading-cards",
          "slug": "steam-trading-cards",
          "language": "eng",
          "games_count": 7582,
          "image_background": "https://media.rawg.io/media/games/46d/46d98e6910fbc0706e2948a7cc9b10c5.jpg"
        },
        {
          "id": 18,
          "name": "Co-op",
          "slug": "co-op",
          "language": "eng",
          "games_count": 8915,
          "image_background": "https://media.rawg.io/media/games/73e/73eecb8909e0c39fb246f457b5d6cbbe.jpg"
        },
        {
          "id": 411,
          "name": "cooperative",
          "slug": "cooperative",
          "language": "eng",
          "games_count": 3604,
          "image_background": "https://media.rawg.io/media/games/baf/baf9905270314e07e6850cffdb51df41.jpg"
        },
        {
          "id": 8,
          "name": "First-Person",
          "slug": "first-person",
          "language": "eng",
          "games_count": 24488,
          "image_background": "https://media.rawg.io/media/games/511/5118aff5091cb3efec399c808f8c598f.jpg"
        },
        {
          "id": 30,
          "name": "FPS",
          "slug": "fps",
          "language": "eng",
          "games_count": 11305,
          "image_background": "https://media.rawg.io/media/games/fc1/fc1307a2774506b5bd65d7e8424664a7.jpg"
        },
        {
          "id": 9,
          "name": "Online Co-Op",
          "slug": "online-co-op",
          "language": "eng",
          "games_count": 3795,
          "image_background": "https://media.rawg.io/media/games/34b/34b1f1850a1c06fd971bc6ab3ac0ce0e.jpg"
        },
        {
          "id": 80,
          "name": "Tactical",
          "slug": "tactical",
          "language": "eng",
          "games_count": 3561,
          "image_background": "https://media.rawg.io/media/games/55e/55ee6432ac2bf224610fa17e4c652107.jpg"
        },
        {
          "id": 11669,
          "name": "stats",
          "slug": "stats",
          "language": "eng",
          "games_count": 4254,
          "image_background": "https://media.rawg.io/media/games/f52/f5206d55f918edf8ee07803101106fa6.jpg"
        },
        {
          "id": 40852,
          "name": "Steam Workshop",
          "slug": "steam-workshop",
          "language": "eng",
          "games_count": 1228,
          "image_background": "https://media.rawg.io/media/games/0fd/0fd84d36596a83ef2e5a35f63a072218.jpg"
        },
        {
          "id": 62,
          "name": "Moddable",
          "slug": "moddable",
          "language": "eng",
          "games_count": 723,
          "image_background": "https://media.rawg.io/media/games/c22/c22d804ac753c72f2617b3708a625dec.jpg"
        },
        {
          "id": 157,
          "name": "PvP",
          "slug": "pvp",
          "language": "eng",
          "games_count": 6153,
          "image_background": "https://media.rawg.io/media/games/539/53911178036df07d518dd492ddc25289.jpg"
        },
        {
          "id": 70,
          "name": "War",
          "slug": "war",
          "language": "eng",
          "games_count": 8209,
          "image_background": "https://media.rawg.io/media/games/0fa/0fadc446fd1e9ae9e23a32793d9a5406.jpg"
        },
        {
          "id": 40837,
          "name": "In-App Purchases",
          "slug": "in-app-purchases",
          "language": "eng",
          "games_count": 1835,
          "image_background": "https://media.rawg.io/media/games/f87/f87457e8347484033cb34cde6101d08d.jpg"
        },
        {
          "id": 11,
          "name": "Team-Based",
          "slug": "team-based",
          "language": "eng",
          "games_count": 1142,
          "image_background": "https://media.rawg.io/media/games/d58/d588947d4286e7b5e0e12e1bea7d9844.jpg"
        },
        {
          "id": 131,
          "name": "Fast-Paced",
          "slug": "fast-paced",
          "language": "eng",
          "games_count": 9772,
          "image_background": "https://media.rawg.io/media/games/9a1/9a18c226cf379272c698f26d2b79b3da.jpg"
        },
        {
          "id": 77,
          "name": "Realistic",
          "slug": "realistic",
          "language": "eng",
          "games_count": 2918,
          "image_background": "https://media.rawg.io/media/games/d07/d0790809a13027251b6d0f4dc7538c58.jpg"
        },
        {
          "id": 81,
          "name": "Military",
          "slug": "military",
          "language": "eng",
          "games_count": 1123,
          "image_background": "https://media.rawg.io/media/games/1a1/1a17e9b6286edb7e1f1e510110ccb0c0.jpg"
        },
        {
          "id": 170,
          "name": "Competitive",
          "slug": "competitive",
          "language": "eng",
          "games_count": 943,
          "image_background": "https://media.rawg.io/media/games/c07/c07e1c1e6908a2eeebd5dad0708abd01.jpg"
        },
        {
          "id": 40856,
          "name": "Valve Anti-Cheat enabled",
          "slug": "valve-anti-cheat-enabled",
          "language": "eng",
          "games_count": 104,
          "image_background": "https://media.rawg.io/media/games/736/73619bd336c894d6941d926bfd563946.jpg"
        },
        {
          "id": 73,
          "name": "e-sports",
          "slug": "e-sports",
          "language": "eng",
          "games_count": 80,
          "image_background": "https://media.rawg.io/media/games/cc7/cc77035eb972f179f5090ee2a0fabd99.jpg"
        },
        {
          "id": 245,
          "name": "Trading",
          "slug": "trading",
          "language": "eng",
          "games_count": 904,
          "image_background": "https://media.rawg.io/media/games/458/4589f56845f867775404f7493810f078.jpg"
        }
      ],
      "esrb_rating": {
        "id": 4,
        "name": "Mature",
        "slug": "mature"
      },
      "short_screenshots": [
        {
          "id": -1,
          "image": "https://media.rawg.io/media/games/736/73619bd336c894d6941d926bfd563946.jpg"
        },
        {
          "id": 81644,
          "image": "https://media.rawg.io/media/screenshots/ff1/ff16661bb15f7969b44f6c4118870e44.jpg"
        },
        {
          "id": 81645,
          "image": "https://media.rawg.io/media/screenshots/41b/41bb769d247412eac3336dec934aed72.jpg"
        },
        {
          "id": 81646,
          "image": "https://media.rawg.io/media/screenshots/754/754545acdbf71f56e8902a07c7d20696.jpg"
        },
        {
          "id": 81647,
          "image": "https://media.rawg.io/media/screenshots/fd8/fd873cab4c66db0b8e85d8a66e940074.jpg"
        },
        {
          "id": 81648,
          "image": "https://media.rawg.io/media/screenshots/7db/7db2954f7908b6749c36a5f3c9052f65.jpg"
        },
        {
          "id": 81649,
          "image": "https://media.rawg.io/media/screenshots/337/337a3e98b5933f456a2b37b59fab5f39.jpg"
        }
      ]
    },
    {
      "id": 5679,
      "slug": "the-elder-scrolls-v-skyrim",
      "name": "The Elder Scrolls V: Skyrim",
      "released": "2011-11-11",
      "tba": false,
      "background_image": "https://media.rawg.io/media/games/7cf/7cfc9220b401b7a300e409e539c9afd5.jpg",
      "rating": 4.42,
      "rating_top": 5,
      "ratings": [
        {
          "id": 5,
          "title": "exceptional",
          "count": 2325,
          "percent": 57.03
        },
        {
          "id": 4,
          "title": "recommended",
          "count": 1301,
          "percent": 31.91
        },
        {
          "id": 3,
          "title": "meh",
          "count": 364,
          "percent": 8.93
        },
        {
          "id": 1,
          "title": "skip",
          "count": 87,
          "percent": 2.13
        }
      ],
      "ratings_count": 4043,
      "reviews_text_count": 23,
      "added": 13653,
      "added_by_status": {
        "yet": 433,
        "owned": 8234,
        "beaten": 3204,
        "toplay": 336,
        "dropped": 1104,
        "playing": 342
      },
      "metacritic": 94,
      "playtime": 46,
      "suggestions_count": 585,
      "updated": "2022-09-28T06:32:13",
      "user_game": null,
      "reviews_count": 4077,
      "saturated_color": "0f0f0f",
      "dominant_color": "0f0f0f",
      "platforms": [
        {
          "platform": {
            "id": 4,
            "name": "PC",
            "slug": "pc",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 483690,
            "image_background": "https://media.rawg.io/media/games/618/618c2031a07bbff6b4f611f10b6bcdbc.jpg"
          },
          "released_at": "2011-11-11",
          "requirements_en": {
            "minimum": "<strong>Minimum:</strong><br>\t\t\t\t\t\t\t\t<ul class=\"bb_ul\"><li><strong>OS:</strong> Windows 7/Vista/XP PC (32 or 64 bit)<br>\t\t\t\t\t\t\t\t</li><li><strong>Processor:</strong> Dual Core 2.0GHz or equivalent processor<br>\t\t\t\t\t\t\t\t</li><li><strong>Memory:</strong> 2GB System RAM<br>\t\t\t\t\t\t\t\t</li><li><strong>Hard Disk Space:</strong> 6GB free HDD Space<br>\t\t\t\t\t\t\t\t</li><li><strong>Video Card:</strong> Direct X 9.0c compliant video card with 512 MB of RAM<br>\t\t\t\t\t\t\t\t</li><li><strong>Sound:</strong> DirectX compatible sound card<br>\t\t\t\t\t\t\t\t</li></ul>",
            "recommended": "<strong>Recommended:</strong><br>\t\t\t\t\t\t\t\t<ul class=\"bb_ul\"><li><strong>Processor:</strong> Quad-core Intel or AMD CPU<br>\t\t\t\t\t\t\t\t</li><li><strong>Memory:</strong> 4GB System RAM<br>\t\t\t\t\t\t\t\t</li><li><strong>Video Card:</strong> DirectX 9.0c compatible NVIDIA or AMD ATI video card with 1GB of RAM (Nvidia GeForce GTX 260 or higher; ATI Radeon 4890 or higher)<br>\t\t\t\t\t\t\t\t</li></ul>"
          },
          "requirements_ru": {
            "minimum": "Core 2 Duo/Athlon X2 2 ГГц,2 Гб памяти,GeForce 8800/Radeon X1900,6 Гб на винчестере",
            "recommended": "Core 2 Quad/Phenom X4 2.5 ГГц,4 Гб памяти,GeForce GTX 260/Radeon HD 4890,6 Гб на винчестере"
          }
        },
        {
          "platform": {
            "id": 7,
            "name": "Nintendo Switch",
            "slug": "nintendo-switch",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 4992,
            "image_background": "https://media.rawg.io/media/games/713/713269608dc8f2f40f5a670a14b2de94.jpg"
          },
          "released_at": "2011-11-11",
          "requirements_en": null,
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 14,
            "name": "Xbox 360",
            "slug": "xbox360",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 2777,
            "image_background": "https://media.rawg.io/media/games/15c/15c95a4915f88a3e89c821526afe05fc.jpg"
          },
          "released_at": "2011-11-11",
          "requirements_en": null,
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 16,
            "name": "PlayStation 3",
            "slug": "playstation3",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 3582,
            "image_background": "https://media.rawg.io/media/games/562/562553814dd54e001a541e4ee83a591c.jpg"
          },
          "released_at": "2011-11-11",
          "requirements_en": null,
          "requirements_ru": null
        }
      ],
      "parent_platforms": [
        {
          "platform": {
            "id": 1,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 2,
            "name": "PlayStation",
            "slug": "playstation"
          }
        },
        {
          "platform": {
            "id": 3,
            "name": "Xbox",
            "slug": "xbox"
          }
        },
        {
          "platform": {
            "id": 7,
            "name": "Nintendo",
            "slug": "nintendo"
          }
        }
      ],
      "genres": [
        {
          "id": 4,
          "name": "Action",
          "slug": "action",
          "games_count": 162171,
          "image_background": "https://media.rawg.io/media/games/511/5118aff5091cb3efec399c808f8c598f.jpg"
        },
        {
          "id": 5,
          "name": "RPG",
          "slug": "role-playing-games-rpg",
          "games_count": 49440,
          "image_background": "https://media.rawg.io/media/games/7cf/7cfc9220b401b7a300e409e539c9afd5.jpg"
        }
      ],
      "stores": [
        {
          "id": 6037,
          "store": {
            "id": 3,
            "name": "PlayStation Store",
            "slug": "playstation-store",
            "domain": "store.playstation.com",
            "games_count": 7797,
            "image_background": "https://media.rawg.io/media/games/f87/f87457e8347484033cb34cde6101d08d.jpg"
          }
        },
        {
          "id": 15144,
          "store": {
            "id": 1,
            "name": "Steam",
            "slug": "steam",
            "domain": "store.steampowered.com",
            "games_count": 67190,
            "image_background": "https://media.rawg.io/media/games/c4b/c4b0cab189e73432de3a250d8cf1c84e.jpg"
          }
        },
        {
          "id": 32919,
          "store": {
            "id": 6,
            "name": "Nintendo Store",
            "slug": "nintendo",
            "domain": "nintendo.com",
            "games_count": 8857,
            "image_background": "https://media.rawg.io/media/games/879/879c930f9c6787c920153fa2df452eb3.jpg"
          }
        },
        {
          "id": 49792,
          "store": {
            "id": 7,
            "name": "Xbox 360 Store",
            "slug": "xbox360",
            "domain": "marketplace.xbox.com",
            "games_count": 1915,
            "image_background": "https://media.rawg.io/media/games/120/1201a40e4364557b124392ee50317b99.jpg"
          }
        }
      ],
      "clip": null,
      "tags": [
        {
          "id": 31,
          "name": "Singleplayer",
          "slug": "singleplayer",
          "language": "eng",
          "games_count": 183363,
          "image_background": "https://media.rawg.io/media/games/588/588c6bdff3d4baf66ec36b1c05b793bf.jpg"
        },
        {
          "id": 40847,
          "name": "Steam Achievements",
          "slug": "steam-achievements",
          "language": "eng",
          "games_count": 27271,
          "image_background": "https://media.rawg.io/media/games/b45/b45575f34285f2c4479c9a5f719d972e.jpg"
        },
        {
          "id": 13,
          "name": "Atmospheric",
          "slug": "atmospheric",
          "language": "eng",
          "games_count": 24853,
          "image_background": "https://media.rawg.io/media/games/b7d/b7d3f1715fa8381a4e780173a197a615.jpg"
        },
        {
          "id": 40849,
          "name": "Steam Cloud",
          "slug": "steam-cloud",
          "language": "eng",
          "games_count": 12450,
          "image_background": "https://media.rawg.io/media/games/310/3106b0e012271c5ffb16497b070be739.jpg"
        },
        {
          "id": 7808,
          "name": "steam-trading-cards",
          "slug": "steam-trading-cards",
          "language": "eng",
          "games_count": 7582,
          "image_background": "https://media.rawg.io/media/games/46d/46d98e6910fbc0706e2948a7cc9b10c5.jpg"
        },
        {
          "id": 42,
          "name": "Great Soundtrack",
          "slug": "great-soundtrack",
          "language": "eng",
          "games_count": 3212,
          "image_background": "https://media.rawg.io/media/games/c4b/c4b0cab189e73432de3a250d8cf1c84e.jpg"
        },
        {
          "id": 24,
          "name": "RPG",
          "slug": "rpg",
          "language": "eng",
          "games_count": 15378,
          "image_background": "https://media.rawg.io/media/games/c6b/c6bfece1daf8d06bc0a60632ac78e5bf.jpg"
        },
        {
          "id": 118,
          "name": "Story Rich",
          "slug": "story-rich",
          "language": "eng",
          "games_count": 16047,
          "image_background": "https://media.rawg.io/media/games/960/960b601d9541cec776c5fa42a00bf6c4.jpg"
        },
        {
          "id": 36,
          "name": "Open World",
          "slug": "open-world",
          "language": "eng",
          "games_count": 5529,
          "image_background": "https://media.rawg.io/media/games/d82/d82990b9c67ba0d2d09d4e6fa88885a7.jpg"
        },
        {
          "id": 8,
          "name": "First-Person",
          "slug": "first-person",
          "language": "eng",
          "games_count": 24488,
          "image_background": "https://media.rawg.io/media/games/511/5118aff5091cb3efec399c808f8c598f.jpg"
        },
        {
          "id": 149,
          "name": "Third Person",
          "slug": "third-person",
          "language": "eng",
          "games_count": 7938,
          "image_background": "https://media.rawg.io/media/games/5c0/5c0dd63002cb23f804aab327d40ef119.jpg"
        },
        {
          "id": 40845,
          "name": "Partial Controller Support",
          "slug": "partial-controller-support",
          "language": "eng",
          "games_count": 8832,
          "image_background": "https://media.rawg.io/media/games/995/9951d9d55323d08967640f7b9ab3e342.jpg"
        },
        {
          "id": 64,
          "name": "Fantasy",
          "slug": "fantasy",
          "language": "eng",
          "games_count": 21547,
          "image_background": "https://media.rawg.io/media/games/4e6/4e6e8e7f50c237d76f38f3c885dae3d2.jpg"
        },
        {
          "id": 37,
          "name": "Sandbox",
          "slug": "sandbox",
          "language": "eng",
          "games_count": 5293,
          "image_background": "https://media.rawg.io/media/games/48c/48cb04ca483be865e3a83119c94e6097.jpg"
        },
        {
          "id": 97,
          "name": "Action RPG",
          "slug": "action-rpg",
          "language": "eng",
          "games_count": 5147,
          "image_background": "https://media.rawg.io/media/games/26d/26d4437715bee60138dab4a7c8c59c92.jpg"
        },
        {
          "id": 40852,
          "name": "Steam Workshop",
          "slug": "steam-workshop",
          "language": "eng",
          "games_count": 1228,
          "image_background": "https://media.rawg.io/media/games/0fd/0fd84d36596a83ef2e5a35f63a072218.jpg"
        },
        {
          "id": 62,
          "name": "Moddable",
          "slug": "moddable",
          "language": "eng",
          "games_count": 723,
          "image_background": "https://media.rawg.io/media/games/c22/c22d804ac753c72f2617b3708a625dec.jpg"
        },
        {
          "id": 468,
          "name": "role-playing",
          "slug": "role-playing",
          "language": "eng",
          "games_count": 1369,
          "image_background": "https://media.rawg.io/media/games/046/0464f4a36cd975a37c95b93b06058855.jpg"
        },
        {
          "id": 121,
          "name": "Character Customization",
          "slug": "character-customization",
          "language": "eng",
          "games_count": 2870,
          "image_background": "https://media.rawg.io/media/games/da1/da1b267764d77221f07a4386b6548e5a.jpg"
        },
        {
          "id": 40,
          "name": "Dark Fantasy",
          "slug": "dark-fantasy",
          "language": "eng",
          "games_count": 2724,
          "image_background": "https://media.rawg.io/media/games/ee3/ee3e10193aafc3230ba1cae426967d10.jpg"
        },
        {
          "id": 66,
          "name": "Medieval",
          "slug": "medieval",
          "language": "eng",
          "games_count": 4589,
          "image_background": "https://media.rawg.io/media/games/c22/c22d804ac753c72f2617b3708a625dec.jpg"
        },
        {
          "id": 82,
          "name": "Magic",
          "slug": "magic",
          "language": "eng",
          "games_count": 7230,
          "image_background": "https://media.rawg.io/media/games/639/639ce7d7fecbb9f0717e9fbc1180f8f8.jpg"
        },
        {
          "id": 205,
          "name": "Lore-Rich",
          "slug": "lore-rich",
          "language": "eng",
          "games_count": 562,
          "image_background": "https://media.rawg.io/media/games/dfe/dfe14d30ed6fccd9db4ae79d3be51d54.jpg"
        },
        {
          "id": 215,
          "name": "Dragons",
          "slug": "dragons",
          "language": "eng",
          "games_count": 2268,
          "image_background": "https://media.rawg.io/media/games/a3c/a3cffe57a6a62287f975079c1c4540d4.jpg"
        }
      ],
      "esrb_rating": {
        "id": 4,
        "name": "Mature",
        "slug": "mature"
      },
      "short_screenshots": [
        {
          "id": -1,
          "image": "https://media.rawg.io/media/games/7cf/7cfc9220b401b7a300e409e539c9afd5.jpg"
        },
        {
          "id": 118307,
          "image": "https://media.rawg.io/media/screenshots/3bd/3bd2710bd1ffb6664fdea7b83afd067e.jpg"
        },
        {
          "id": 118308,
          "image": "https://media.rawg.io/media/screenshots/d4e/d4e9b13f54748584ccbd6f998094dade.jpg"
        },
        {
          "id": 118309,
          "image": "https://media.rawg.io/media/screenshots/599/59946a630e9c7871003763d63184404a.jpg"
        },
        {
          "id": 118310,
          "image": "https://media.rawg.io/media/screenshots/c5d/c5dad426038d7d12f933eedbeab48ff3.jpg"
        },
        {
          "id": 118311,
          "image": "https://media.rawg.io/media/screenshots/b32/b326fa01c82db82edd41ed299886ee44.jpg"
        },
        {
          "id": 118312,
          "image": "https://media.rawg.io/media/screenshots/091/091e95b49d5a22de036698fc731395a2.jpg"
        }
      ]
    },
    {
      "id": 12020,
      "slug": "left-4-dead-2",
      "name": "Left 4 Dead 2",
      "released": "2009-11-17",
      "tba": false,
      "background_image": "https://media.rawg.io/media/games/d58/d588947d4286e7b5e0e12e1bea7d9844.jpg",
      "rating": 4.08,
      "rating_top": 4,
      "ratings": [
        {
          "id": 4,
          "title": "recommended",
          "count": 1536,
          "percent": 53.41
        },
        {
          "id": 5,
          "title": "exceptional",
          "count": 896,
          "percent": 31.15
        },
        {
          "id": 3,
          "title": "meh",
          "count": 336,
          "percent": 11.68
        },
        {
          "id": 1,
          "title": "skip",
          "count": 108,
          "percent": 3.76
        }
      ],
      "ratings_count": 2861,
      "reviews_text_count": 8,
      "added": 13522,
      "added_by_status": {
        "yet": 337,
        "owned": 9864,
        "beaten": 2140,
        "toplay": 86,
        "dropped": 967,
        "playing": 128
      },
      "metacritic": 89,
      "playtime": 9,
      "suggestions_count": 581,
      "updated": "2022-09-28T06:32:54",
      "user_game": null,
      "reviews_count": 2876,
      "saturated_color": "0f0f0f",
      "dominant_color": "0f0f0f",
      "platforms": [
        {
          "platform": {
            "id": 5,
            "name": "macOS",
            "slug": "macos",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 98332,
            "image_background": "https://media.rawg.io/media/games/b54/b54598d1d5cc31899f4f0a7e3122a7b0.jpg"
          },
          "released_at": "2009-11-17",
          "requirements_en": null,
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 6,
            "name": "Linux",
            "slug": "linux",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 72202,
            "image_background": "https://media.rawg.io/media/games/9dd/9ddabb34840ea9227556670606cf8ea3.jpg"
          },
          "released_at": "2009-11-17",
          "requirements_en": null,
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 4,
            "name": "PC",
            "slug": "pc",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 483690,
            "image_background": "https://media.rawg.io/media/games/618/618c2031a07bbff6b4f611f10b6bcdbc.jpg"
          },
          "released_at": "2009-11-17",
          "requirements_en": {
            "minimum": "<strong>Minimum:</strong><br><ul class=\"bb_ul\"><li><strong>OS:</strong> Windows® 7 32/64-bit / Vista 32/64 / XP<br></li><li><strong>Processor:</strong> Pentium 4 3.0GHz<br></li><li><strong>Memory:</strong> 2 GB RAM<br></li><li><strong>Graphics:</strong> Video card with 128 MB, Shader model 2.0. ATI X800, NVidia 6600 or better<br></li><li><strong>DirectX:</strong> Version 9.0c<br></li><li><strong>Storage:</strong> 13 GB available space<br></li><li><strong>Sound Card:</strong> DirectX 9.0c compatible sound card</li></ul>",
            "recommended": "<strong>Recommended:</strong><br><ul class=\"bb_ul\"><li><strong>OS:</strong> Windows® 7 32/64-bit / Vista 32/64 / XP<br></li><li><strong>Processor:</strong> Intel core 2 duo 2.4GHz<br></li><li><strong>Memory:</strong> 2 GB RAM<br></li><li><strong>Graphics:</strong> Video Card Shader model 3.0. NVidia 7600, ATI X1600 or better<br></li><li><strong>DirectX:</strong> Version 9.0c<br></li><li><strong>Storage:</strong> 13 GB available space<br></li><li><strong>Sound Card:</strong> DirectX 9.0c compatible sound card</li></ul>"
          },
          "requirements_ru": {
            "minimum": "Pentium 4/Athlon 64 3 ГГц,1 Гб памяти,GeForce 6600/Radeon X800,7.5 Гб на винчестере",
            "recommended": "Core 2 Duo/Athlon 64 X2 2.4 ГГц,2 Гб памяти,GeForce 7600/Radeon X1600,7.5 Гб на винчестере"
          }
        },
        {
          "platform": {
            "id": 14,
            "name": "Xbox 360",
            "slug": "xbox360",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 2777,
            "image_background": "https://media.rawg.io/media/games/15c/15c95a4915f88a3e89c821526afe05fc.jpg"
          },
          "released_at": "2009-11-17",
          "requirements_en": null,
          "requirements_ru": null
        }
      ],
      "parent_platforms": [
        {
          "platform": {
            "id": 1,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 3,
            "name": "Xbox",
            "slug": "xbox"
          }
        },
        {
          "platform": {
            "id": 5,
            "name": "Apple Macintosh",
            "slug": "mac"
          }
        },
        {
          "platform": {
            "id": 6,
            "name": "Linux",
            "slug": "linux"
          }
        }
      ],
      "genres": [
        {
          "id": 4,
          "name": "Action",
          "slug": "action",
          "games_count": 162171,
          "image_background": "https://media.rawg.io/media/games/511/5118aff5091cb3efec399c808f8c598f.jpg"
        },
        {
          "id": 2,
          "name": "Shooter",
          "slug": "shooter",
          "games_count": 56671,
          "image_background": "https://media.rawg.io/media/games/34b/34b1f1850a1c06fd971bc6ab3ac0ce0e.jpg"
        }
      ],
      "stores": [
        {
          "id": 13208,
          "store": {
            "id": 1,
            "name": "Steam",
            "slug": "steam",
            "domain": "store.steampowered.com",
            "games_count": 67190,
            "image_background": "https://media.rawg.io/media/games/c4b/c4b0cab189e73432de3a250d8cf1c84e.jpg"
          }
        },
        {
          "id": 34000,
          "store": {
            "id": 7,
            "name": "Xbox 360 Store",
            "slug": "xbox360",
            "domain": "marketplace.xbox.com",
            "games_count": 1915,
            "image_background": "https://media.rawg.io/media/games/120/1201a40e4364557b124392ee50317b99.jpg"
          }
        }
      ],
      "clip": null,
      "tags": [
        {
          "id": 31,
          "name": "Singleplayer",
          "slug": "singleplayer",
          "language": "eng",
          "games_count": 183363,
          "image_background": "https://media.rawg.io/media/games/588/588c6bdff3d4baf66ec36b1c05b793bf.jpg"
        },
        {
          "id": 40847,
          "name": "Steam Achievements",
          "slug": "steam-achievements",
          "language": "eng",
          "games_count": 27271,
          "image_background": "https://media.rawg.io/media/games/b45/b45575f34285f2c4479c9a5f719d972e.jpg"
        },
        {
          "id": 7,
          "name": "Multiplayer",
          "slug": "multiplayer",
          "language": "eng",
          "games_count": 32955,
          "image_background": "https://media.rawg.io/media/games/328/3283617cb7d75d67257fc58339188742.jpg"
        },
        {
          "id": 40836,
          "name": "Full controller support",
          "slug": "full-controller-support",
          "language": "eng",
          "games_count": 12760,
          "image_background": "https://media.rawg.io/media/games/b49/b4912b5dbfc7ed8927b65f05b8507f6c.jpg"
        },
        {
          "id": 40849,
          "name": "Steam Cloud",
          "slug": "steam-cloud",
          "language": "eng",
          "games_count": 12450,
          "image_background": "https://media.rawg.io/media/games/310/3106b0e012271c5ffb16497b070be739.jpg"
        },
        {
          "id": 7808,
          "name": "steam-trading-cards",
          "slug": "steam-trading-cards",
          "language": "eng",
          "games_count": 7582,
          "image_background": "https://media.rawg.io/media/games/46d/46d98e6910fbc0706e2948a7cc9b10c5.jpg"
        },
        {
          "id": 18,
          "name": "Co-op",
          "slug": "co-op",
          "language": "eng",
          "games_count": 8915,
          "image_background": "https://media.rawg.io/media/games/73e/73eecb8909e0c39fb246f457b5d6cbbe.jpg"
        },
        {
          "id": 411,
          "name": "cooperative",
          "slug": "cooperative",
          "language": "eng",
          "games_count": 3604,
          "image_background": "https://media.rawg.io/media/games/baf/baf9905270314e07e6850cffdb51df41.jpg"
        },
        {
          "id": 8,
          "name": "First-Person",
          "slug": "first-person",
          "language": "eng",
          "games_count": 24488,
          "image_background": "https://media.rawg.io/media/games/511/5118aff5091cb3efec399c808f8c598f.jpg"
        },
        {
          "id": 16,
          "name": "Horror",
          "slug": "horror",
          "language": "eng",
          "games_count": 37098,
          "image_background": "https://media.rawg.io/media/games/198/1988a337305e008b41d7f536ce9b73f6.jpg"
        },
        {
          "id": 30,
          "name": "FPS",
          "slug": "fps",
          "language": "eng",
          "games_count": 11305,
          "image_background": "https://media.rawg.io/media/games/fc1/fc1307a2774506b5bd65d7e8424664a7.jpg"
        },
        {
          "id": 9,
          "name": "Online Co-Op",
          "slug": "online-co-op",
          "language": "eng",
          "games_count": 3795,
          "image_background": "https://media.rawg.io/media/games/34b/34b1f1850a1c06fd971bc6ab3ac0ce0e.jpg"
        },
        {
          "id": 26,
          "name": "Gore",
          "slug": "gore",
          "language": "eng",
          "games_count": 4653,
          "image_background": "https://media.rawg.io/media/games/c80/c80bcf321da44d69b18a06c04d942662.jpg"
        },
        {
          "id": 1,
          "name": "Survival",
          "slug": "survival",
          "language": "eng",
          "games_count": 6384,
          "image_background": "https://media.rawg.io/media/games/5c0/5c0dd63002cb23f804aab327d40ef119.jpg"
        },
        {
          "id": 75,
          "name": "Local Co-Op",
          "slug": "local-co-op",
          "language": "eng",
          "games_count": 4669,
          "image_background": "https://media.rawg.io/media/games/c6b/c6bfece1daf8d06bc0a60632ac78e5bf.jpg"
        },
        {
          "id": 80,
          "name": "Tactical",
          "slug": "tactical",
          "language": "eng",
          "games_count": 3561,
          "image_background": "https://media.rawg.io/media/games/55e/55ee6432ac2bf224610fa17e4c652107.jpg"
        },
        {
          "id": 11669,
          "name": "stats",
          "slug": "stats",
          "language": "eng",
          "games_count": 4254,
          "image_background": "https://media.rawg.io/media/games/f52/f5206d55f918edf8ee07803101106fa6.jpg"
        },
        {
          "id": 40852,
          "name": "Steam Workshop",
          "slug": "steam-workshop",
          "language": "eng",
          "games_count": 1228,
          "image_background": "https://media.rawg.io/media/games/0fd/0fd84d36596a83ef2e5a35f63a072218.jpg"
        },
        {
          "id": 63,
          "name": "Zombies",
          "slug": "zombies",
          "language": "eng",
          "games_count": 9128,
          "image_background": "https://media.rawg.io/media/games/410/41033a495ce8f7fd4b0934bdb975f12a.jpg"
        },
        {
          "id": 62,
          "name": "Moddable",
          "slug": "moddable",
          "language": "eng",
          "games_count": 723,
          "image_background": "https://media.rawg.io/media/games/c22/c22d804ac753c72f2617b3708a625dec.jpg"
        },
        {
          "id": 43,
          "name": "Post-apocalyptic",
          "slug": "post-apocalyptic",
          "language": "eng",
          "games_count": 2872,
          "image_background": "https://media.rawg.io/media/games/93e/93ee6101e1c943732f06080dddb0fe4c.jpg"
        },
        {
          "id": 40833,
          "name": "Captions available",
          "slug": "captions-available",
          "language": "eng",
          "games_count": 1196,
          "image_background": "https://media.rawg.io/media/games/33b/33b825c76382931df0fd8ecddf5caebe.jpg"
        },
        {
          "id": 5,
          "name": "Replay Value",
          "slug": "replay-value",
          "language": "eng",
          "games_count": 1142,
          "image_background": "https://media.rawg.io/media/games/511/5116b4524cea34c6b3f12e0ca027d850.jpg"
        },
        {
          "id": 17,
          "name": "Survival Horror",
          "slug": "survival-horror",
          "language": "eng",
          "games_count": 6889,
          "image_background": "https://media.rawg.io/media/games/348/348640e78a7fcd4bb7dcad4fea014eeb.jpg"
        },
        {
          "id": 11,
          "name": "Team-Based",
          "slug": "team-based",
          "language": "eng",
          "games_count": 1142,
          "image_background": "https://media.rawg.io/media/games/d58/d588947d4286e7b5e0e12e1bea7d9844.jpg"
        },
        {
          "id": 40856,
          "name": "Valve Anti-Cheat enabled",
          "slug": "valve-anti-cheat-enabled",
          "language": "eng",
          "games_count": 104,
          "image_background": "https://media.rawg.io/media/games/736/73619bd336c894d6941d926bfd563946.jpg"
        },
        {
          "id": 40834,
          "name": "Commentary available",
          "slug": "commentary-available",
          "language": "eng",
          "games_count": 248,
          "image_background": "https://media.rawg.io/media/games/726/7263e11b6bfb15abe35dcfa3b26147f5.jpg"
        },
        {
          "id": 40839,
          "name": "Includes Source SDK",
          "slug": "includes-source-sdk",
          "language": "eng",
          "games_count": 56,
          "image_background": "https://media.rawg.io/media/screenshots/3b0/3b0240fb639701e6008e9c37f2db0a2c.jpg"
        }
      ],
      "esrb_rating": {
        "id": 4,
        "name": "Mature",
        "slug": "mature"
      },
      "short_screenshots": [
        {
          "id": -1,
          "image": "https://media.rawg.io/media/games/d58/d588947d4286e7b5e0e12e1bea7d9844.jpg"
        },
        {
          "id": 99748,
          "image": "https://media.rawg.io/media/screenshots/4c0/4c043fd1a5ff78900483f2e82580fea0.jpg"
        },
        {
          "id": 99749,
          "image": "https://media.rawg.io/media/screenshots/c90/c9071628c238fbc20b366e2413dd8b4a.jpg"
        },
        {
          "id": 99750,
          "image": "https://media.rawg.io/media/screenshots/e29/e293b0f98092b8c0dbe24d66846088bb.jpg"
        },
        {
          "id": 99751,
          "image": "https://media.rawg.io/media/screenshots/168/16867bc76b385eb0fb749e41f7ada93d.jpg"
        },
        {
          "id": 99752,
          "image": "https://media.rawg.io/media/screenshots/fb9/fb917e562f311f48ff8d27632bd29a80.jpg"
        },
        {
          "id": 99753,
          "image": "https://media.rawg.io/media/screenshots/5f2/5f2ca569912add2a211b20ba3f576b97.jpg"
        }
      ]
    },
    {
      "id": 13536,
      "slug": "portal",
      "name": "Portal",
      "released": "2007-10-09",
      "tba": false,
      "background_image": "https://media.rawg.io/media/games/7fa/7fa0b586293c5861ee32490e953a4996.jpg",
      "rating": 4.51,
      "rating_top": 5,
      "ratings": [
        {
          "id": 5,
          "title": "exceptional",
          "count": 2498,
          "percent": 60.88
        },
        {
          "id": 4,
          "title": "recommended",
          "count": 1358,
          "percent": 33.1
        },
        {
          "id": 3,
          "title": "meh",
          "count": 164,
          "percent": 4.0
        },
        {
          "id": 1,
          "title": "skip",
          "count": 83,
          "percent": 2.02
        }
      ],
      "ratings_count": 4072,
      "reviews_text_count": 22,
      "added": 13428,
      "added_by_status": {
        "yet": 378,
        "owned": 8262,
        "beaten": 4202,
        "toplay": 206,
        "dropped": 315,
        "playing": 65
      },
      "metacritic": 90,
      "playtime": 4,
      "suggestions_count": 284,
      "updated": "2022-09-28T06:09:16",
      "user_game": null,
      "reviews_count": 4103,
      "saturated_color": "0f0f0f",
      "dominant_color": "0f0f0f",
      "platforms": [
        {
          "platform": {
            "id": 4,
            "name": "PC",
            "slug": "pc",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 483690,
            "image_background": "https://media.rawg.io/media/games/618/618c2031a07bbff6b4f611f10b6bcdbc.jpg"
          },
          "released_at": "2007-10-09",
          "requirements_en": {
            "minimum": "<p><strong>Minimum: </strong>1.7 GHz Processor, 512MB RAM, DirectX&reg; 8.1 level Graphics Card (Requires support for SSE), Windows&reg; 7 (32/64-bit)/Vista/XP, Mouse, Keyboard, Internet Connection</p>\r\n\t\t\t<p><strong>Recommended: </strong>Pentium 4 processor (3.0GHz, or better), 1GB RAM, DirectX&reg; 9 level Graphics Card, Windows&reg; 7 (32/64-bit)/Vista/XP, Mouse, Keyboard, Internet Connection</p>"
          },
          "requirements_ru": {
            "minimum": "Pentium 4/Athlon XP 1.7 ГГц,512 Мб памяти,3D-ускоритель со 128 Мб памяти,7.5 Гб на винчестере,Интернет-соединение 128 Кб/c",
            "recommended": "Core 2 Duo/Athlon 64 3 ГГц,1 Гб памяти,3D-ускоритель с 512 Мб памяти,12 Гб на винчестере,Интернет-соединение 512 Кб/c"
          }
        },
        {
          "platform": {
            "id": 5,
            "name": "macOS",
            "slug": "macos",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 98332,
            "image_background": "https://media.rawg.io/media/games/b54/b54598d1d5cc31899f4f0a7e3122a7b0.jpg"
          },
          "released_at": "2007-10-09",
          "requirements_en": {
            "minimum": "<strong>Minimum: </strong>OS X version Leopard 10.5.8, Snow Leopard 10.6.3, 1GB RAM, NVIDIA GeForce 8 or higher, ATI X1600 or higher, or Intel HD 3000 or higher Mouse, Keyboard, Internet Connection"
          },
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 6,
            "name": "Linux",
            "slug": "linux",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 72202,
            "image_background": "https://media.rawg.io/media/games/9dd/9ddabb34840ea9227556670606cf8ea3.jpg"
          },
          "released_at": "2007-10-09",
          "requirements_en": null,
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 14,
            "name": "Xbox 360",
            "slug": "xbox360",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 2777,
            "image_background": "https://media.rawg.io/media/games/15c/15c95a4915f88a3e89c821526afe05fc.jpg"
          },
          "released_at": "2007-10-09",
          "requirements_en": null,
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 16,
            "name": "PlayStation 3",
            "slug": "playstation3",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 3582,
            "image_background": "https://media.rawg.io/media/games/562/562553814dd54e001a541e4ee83a591c.jpg"
          },
          "released_at": "2007-10-09",
          "requirements_en": null,
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 21,
            "name": "Android",
            "slug": "android",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 50878,
            "image_background": "https://media.rawg.io/media/games/c7a/c7a71a0531a9518236d99d0d60abe447.jpg"
          },
          "released_at": "2007-10-09",
          "requirements_en": {
            "minimum": "4.4 and up"
          },
          "requirements_ru": null
        }
      ],
      "parent_platforms": [
        {
          "platform": {
            "id": 1,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 2,
            "name": "PlayStation",
            "slug": "playstation"
          }
        },
        {
          "platform": {
            "id": 3,
            "name": "Xbox",
            "slug": "xbox"
          }
        },
        {
          "platform": {
            "id": 8,
            "name": "Android",
            "slug": "android"
          }
        },
        {
          "platform": {
            "id": 5,
            "name": "Apple Macintosh",
            "slug": "mac"
          }
        },
        {
          "platform": {
            "id": 6,
            "name": "Linux",
            "slug": "linux"
          }
        }
      ],
      "genres": [
        {
          "id": 3,
          "name": "Adventure",
          "slug": "adventure",
          "games_count": 123284,
          "image_background": "https://media.rawg.io/media/games/b54/b54598d1d5cc31899f4f0a7e3122a7b0.jpg"
        },
        {
          "id": 7,
          "name": "Puzzle",
          "slug": "puzzle",
          "games_count": 91877,
          "image_background": "https://media.rawg.io/media/games/6fc/6fcb1c529c764700d55f3bbc1b0fbb5b.jpg"
        }
      ],
      "stores": [
        {
          "id": 14890,
          "store": {
            "id": 1,
            "name": "Steam",
            "slug": "steam",
            "domain": "store.steampowered.com",
            "games_count": 67190,
            "image_background": "https://media.rawg.io/media/games/c4b/c4b0cab189e73432de3a250d8cf1c84e.jpg"
          }
        },
        {
          "id": 40973,
          "store": {
            "id": 8,
            "name": "Google Play",
            "slug": "google-play",
            "domain": "play.google.com",
            "games_count": 16868,
            "image_background": "https://media.rawg.io/media/games/8d6/8d69eb6c32ed6acfd75f82d532144993.jpg"
          }
        }
      ],
      "clip": null,
      "tags": [
        {
          "id": 31,
          "name": "Singleplayer",
          "slug": "singleplayer",
          "language": "eng",
          "games_count": 183363,
          "image_background": "https://media.rawg.io/media/games/588/588c6bdff3d4baf66ec36b1c05b793bf.jpg"
        },
        {
          "id": 40847,
          "name": "Steam Achievements",
          "slug": "steam-achievements",
          "language": "eng",
          "games_count": 27271,
          "image_background": "https://media.rawg.io/media/games/b45/b45575f34285f2c4479c9a5f719d972e.jpg"
        },
        {
          "id": 13,
          "name": "Atmospheric",
          "slug": "atmospheric",
          "language": "eng",
          "games_count": 24853,
          "image_background": "https://media.rawg.io/media/games/b7d/b7d3f1715fa8381a4e780173a197a615.jpg"
        },
        {
          "id": 42,
          "name": "Great Soundtrack",
          "slug": "great-soundtrack",
          "language": "eng",
          "games_count": 3212,
          "image_background": "https://media.rawg.io/media/games/c4b/c4b0cab189e73432de3a250d8cf1c84e.jpg"
        },
        {
          "id": 118,
          "name": "Story Rich",
          "slug": "story-rich",
          "language": "eng",
          "games_count": 16047,
          "image_background": "https://media.rawg.io/media/games/960/960b601d9541cec776c5fa42a00bf6c4.jpg"
        },
        {
          "id": 8,
          "name": "First-Person",
          "slug": "first-person",
          "language": "eng",
          "games_count": 24488,
          "image_background": "https://media.rawg.io/media/games/511/5118aff5091cb3efec399c808f8c598f.jpg"
        },
        {
          "id": 32,
          "name": "Sci-fi",
          "slug": "sci-fi",
          "language": "eng",
          "games_count": 15625,
          "image_background": "https://media.rawg.io/media/games/b72/b7233d5d5b1e75e86bb860ccc7aeca85.jpg"
        },
        {
          "id": 40845,
          "name": "Partial Controller Support",
          "slug": "partial-controller-support",
          "language": "eng",
          "games_count": 8832,
          "image_background": "https://media.rawg.io/media/games/995/9951d9d55323d08967640f7b9ab3e342.jpg"
        },
        {
          "id": 30,
          "name": "FPS",
          "slug": "fps",
          "language": "eng",
          "games_count": 11305,
          "image_background": "https://media.rawg.io/media/games/fc1/fc1307a2774506b5bd65d7e8424664a7.jpg"
        },
        {
          "id": 4,
          "name": "Funny",
          "slug": "funny",
          "language": "eng",
          "games_count": 20437,
          "image_background": "https://media.rawg.io/media/games/10d/10d19e52e5e8415d16a4d344fe711874.jpg"
        },
        {
          "id": 193,
          "name": "Classic",
          "slug": "classic",
          "language": "eng",
          "games_count": 1711,
          "image_background": "https://media.rawg.io/media/games/5fa/5fae5fec3c943179e09da67a4427d68f.jpg"
        },
        {
          "id": 189,
          "name": "Female Protagonist",
          "slug": "female-protagonist",
          "language": "eng",
          "games_count": 9211,
          "image_background": "https://media.rawg.io/media/games/62c/62c7c8b28a27b83680b22fb9d33fc619.jpg"
        },
        {
          "id": 123,
          "name": "Comedy",
          "slug": "comedy",
          "language": "eng",
          "games_count": 9740,
          "image_background": "https://media.rawg.io/media/games/c89/c89ca70716080733d03724277df2c6c7.jpg"
        },
        {
          "id": 40838,
          "name": "Includes level editor",
          "slug": "includes-level-editor",
          "language": "eng",
          "games_count": 1541,
          "image_background": "https://media.rawg.io/media/games/12e/12ea6b35b65df38258e25885a0a392a6.jpg"
        },
        {
          "id": 111,
          "name": "Short",
          "slug": "short",
          "language": "eng",
          "games_count": 50413,
          "image_background": "https://media.rawg.io/media/games/7ac/7aca7ccf0e70cd0974cb899ab9e5158e.jpg"
        },
        {
          "id": 40833,
          "name": "Captions available",
          "slug": "captions-available",
          "language": "eng",
          "games_count": 1196,
          "image_background": "https://media.rawg.io/media/games/33b/33b825c76382931df0fd8ecddf5caebe.jpg"
        },
        {
          "id": 114,
          "name": "Physics",
          "slug": "physics",
          "language": "eng",
          "games_count": 16724,
          "image_background": "https://media.rawg.io/media/screenshots/570/5704316c673fab6994db582e0f43f924.jpg"
        },
        {
          "id": 148,
          "name": "Dark Humor",
          "slug": "dark-humor",
          "language": "eng",
          "games_count": 2202,
          "image_background": "https://media.rawg.io/media/screenshots/ca8/ca840f2a8ebfc74aac1688367dc1f903.jpg"
        },
        {
          "id": 40834,
          "name": "Commentary available",
          "slug": "commentary-available",
          "language": "eng",
          "games_count": 248,
          "image_background": "https://media.rawg.io/media/games/726/7263e11b6bfb15abe35dcfa3b26147f5.jpg"
        },
        {
          "id": 40839,
          "name": "Includes Source SDK",
          "slug": "includes-source-sdk",
          "language": "eng",
          "games_count": 56,
          "image_background": "https://media.rawg.io/media/screenshots/3b0/3b0240fb639701e6008e9c37f2db0a2c.jpg"
        },
        {
          "id": 87,
          "name": "Science",
          "slug": "science",
          "language": "eng",
          "games_count": 1392,
          "image_background": "https://media.rawg.io/media/screenshots/0c3/0c383a754af823c31204533b16fdd31a.jpg"
        }
      ],
      "esrb_rating": {
        "id": 3,
        "name": "Teen",
        "slug": "teen"
      },
      "short_screenshots": [
        {
          "id": -1,
          "image": "https://media.rawg.io/media/games/7fa/7fa0b586293c5861ee32490e953a4996.jpg"
        },
        {
          "id": 115793,
          "image": "https://media.rawg.io/media/screenshots/99e/99e94bd55eb75fa6e75c3dcbb1a570b2.jpg"
        },
        {
          "id": 115794,
          "image": "https://media.rawg.io/media/screenshots/2f0/2f0297a46934d9fa914c626902b1ce20.jpg"
        },
        {
          "id": 115795,
          "image": "https://media.rawg.io/media/screenshots/3b3/3b3713fbca6194dfc4d6e8a8d006d354.jpg"
        },
        {
          "id": 115796,
          "image": "https://media.rawg.io/media/screenshots/c6f/c6f9afc3e4dd51068b22f04acbc6ca99.jpg"
        },
        {
          "id": 115797,
          "image": "https://media.rawg.io/media/screenshots/748/74841207eec76ebc7fc03210168bfb7e.jpg"
        },
        {
          "id": 115798,
          "image": "https://media.rawg.io/media/screenshots/e72/e7256b4caedf099bcb8ebd332f892334.jpg"
        }
      ]
    },
    {
      "id": 4062,
      "slug": "bioshock-infinite",
      "name": "BioShock Infinite",
      "released": "2013-03-26",
      "tba": false,
      "background_image": "https://media.rawg.io/media/games/fc1/fc1307a2774506b5bd65d7e8424664a7.jpg",
      "rating": 4.39,
      "rating_top": 5,
      "ratings": [
        {
          "id": 5,
          "title": "exceptional",
          "count": 1985,
          "percent": 55.28
        },
        {
          "id": 4,
          "title": "recommended",
          "count": 1200,
          "percent": 33.42
        },
        {
          "id": 3,
          "title": "meh",
          "count": 311,
          "percent": 8.66
        },
        {
          "id": 1,
          "title": "skip",
          "count": 95,
          "percent": 2.65
        }
      ],
      "ratings_count": 3560,
      "reviews_text_count": 20,
      "added": 13123,
      "added_by_status": {
        "yet": 709,
        "owned": 7842,
        "beaten": 3753,
        "toplay": 325,
        "dropped": 407,
        "playing": 87
      },
      "metacritic": 94,
      "playtime": 12,
      "suggestions_count": 591,
      "updated": "2022-09-28T06:50:58",
      "user_game": null,
      "reviews_count": 3591,
      "saturated_color": "0f0f0f",
      "dominant_color": "0f0f0f",
      "platforms": [
        {
          "platform": {
            "id": 18,
            "name": "PlayStation 4",
            "slug": "playstation4",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 6479,
            "image_background": "https://media.rawg.io/media/games/fc1/fc1307a2774506b5bd65d7e8424664a7.jpg"
          },
          "released_at": "2013-03-26",
          "requirements_en": null,
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 14,
            "name": "Xbox 360",
            "slug": "xbox360",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 2777,
            "image_background": "https://media.rawg.io/media/games/15c/15c95a4915f88a3e89c821526afe05fc.jpg"
          },
          "released_at": "2013-03-26",
          "requirements_en": null,
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 7,
            "name": "Nintendo Switch",
            "slug": "nintendo-switch",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 4992,
            "image_background": "https://media.rawg.io/media/games/713/713269608dc8f2f40f5a670a14b2de94.jpg"
          },
          "released_at": "2013-03-26",
          "requirements_en": null,
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 6,
            "name": "Linux",
            "slug": "linux",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 72202,
            "image_background": "https://media.rawg.io/media/games/9dd/9ddabb34840ea9227556670606cf8ea3.jpg"
          },
          "released_at": "2013-03-26",
          "requirements_en": null,
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 4,
            "name": "PC",
            "slug": "pc",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 483690,
            "image_background": "https://media.rawg.io/media/games/618/618c2031a07bbff6b4f611f10b6bcdbc.jpg"
          },
          "released_at": "2013-03-26",
          "requirements_en": {
            "minimum": "<strong>Minimum:</strong><br><ul class=\"bb_ul\"><li><strong>OS:</strong> Windows Vista Service Pack 2 32-bit<br>\t</li><li><strong>Processor:</strong> Intel Core 2 DUO 2.4 GHz / AMD Athlon X2 2.7 GHz<br>\t</li><li><strong>Memory:</strong> 2GB<br>\t</li><li><strong>Hard Disk Space:</strong> 20 GB free<br>\t</li><li><strong>Video Card:</strong> DirectX10 Compatible ATI Radeon HD 3870 / NVIDIA 8800 GT / Intel HD 3000 Integrated Graphics<br>\t</li><li><strong>Video Card Memory:</strong> 512 MB\t<br>\t</li><li><strong>Sound:</strong> DirectX Compatible</li></ul>",
            "recommended": "<strong>Recommended:</strong><br><ul class=\"bb_ul\"><li><strong>OS:</strong> Windows 7 Service Pack 1 64-bit<br>\t</li><li><strong>Processor:</strong> Quad Core Processor<br>\t</li><li><strong>Memory:</strong> 4GB<br>\t</li><li><strong>Hard Disk Space:</strong> 30 GB free<br>\t</li><li><strong>Video Card:</strong> DirectX11 Compatible, AMD Radeon HD 6950 / NVIDIA GeForce GTX 560<br>\t</li><li><strong>Video Card Memory:</strong> 1024 MB\t<br>\t</li><li><strong>Sound:</strong> DirectX Compatible</li></ul>"
          },
          "requirements_ru": {
            "minimum": "Win Vista 32\nCore 2 Duo E4600/Athlon 64 X2 5200+\nGeForce GT 340/Radeon HD 3800\n2 GB RAM\n20 GB HDD",
            "recommended": "Win 7 64\nCore 2 Quad Q6600/Athlon II X4 610e\nGeForce GTX 560/Radeon HD 6950\n4 GB RAM\n20 GB HDD"
          }
        },
        {
          "platform": {
            "id": 16,
            "name": "PlayStation 3",
            "slug": "playstation3",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 3582,
            "image_background": "https://media.rawg.io/media/games/562/562553814dd54e001a541e4ee83a591c.jpg"
          },
          "released_at": "2013-03-26",
          "requirements_en": null,
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 1,
            "name": "Xbox One",
            "slug": "xbox-one",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 5399,
            "image_background": "https://media.rawg.io/media/games/490/49016e06ae2103881ff6373248843069.jpg"
          },
          "released_at": "2013-03-26",
          "requirements_en": null,
          "requirements_ru": null
        }
      ],
      "parent_platforms": [
        {
          "platform": {
            "id": 1,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 2,
            "name": "PlayStation",
            "slug": "playstation"
          }
        },
        {
          "platform": {
            "id": 3,
            "name": "Xbox",
            "slug": "xbox"
          }
        },
        {
          "platform": {
            "id": 6,
            "name": "Linux",
            "slug": "linux"
          }
        },
        {
          "platform": {
            "id": 7,
            "name": "Nintendo",
            "slug": "nintendo"
          }
        }
      ],
      "genres": [
        {
          "id": 4,
          "name": "Action",
          "slug": "action",
          "games_count": 162171,
          "image_background": "https://media.rawg.io/media/games/511/5118aff5091cb3efec399c808f8c598f.jpg"
        },
        {
          "id": 2,
          "name": "Shooter",
          "slug": "shooter",
          "games_count": 56671,
          "image_background": "https://media.rawg.io/media/games/34b/34b1f1850a1c06fd971bc6ab3ac0ce0e.jpg"
        }
      ],
      "stores": [
        {
          "id": 828870,
          "store": {
            "id": 11,
            "name": "Epic Games",
            "slug": "epic-games",
            "domain": "epicgames.com",
            "games_count": 1091,
            "image_background": "https://media.rawg.io/media/games/951/951572a3dd1e42544bd39a5d5b42d234.jpg"
          }
        },
        {
          "id": 71727,
          "store": {
            "id": 4,
            "name": "App Store",
            "slug": "apple-appstore",
            "domain": "apps.apple.com",
            "games_count": 73767,
            "image_background": "https://media.rawg.io/media/games/713/713269608dc8f2f40f5a670a14b2de94.jpg"
          }
        },
        {
          "id": 440409,
          "store": {
            "id": 2,
            "name": "Xbox Store",
            "slug": "xbox-store",
            "domain": "microsoft.com",
            "games_count": 4752,
            "image_background": "https://media.rawg.io/media/games/4cf/4cfc6b7f1850590a4634b08bfab308ab.jpg"
          }
        },
        {
          "id": 461035,
          "store": {
            "id": 6,
            "name": "Nintendo Store",
            "slug": "nintendo",
            "domain": "nintendo.com",
            "games_count": 8857,
            "image_background": "https://media.rawg.io/media/games/879/879c930f9c6787c920153fa2df452eb3.jpg"
          }
        },
        {
          "id": 4382,
          "store": {
            "id": 3,
            "name": "PlayStation Store",
            "slug": "playstation-store",
            "domain": "store.playstation.com",
            "games_count": 7797,
            "image_background": "https://media.rawg.io/media/games/f87/f87457e8347484033cb34cde6101d08d.jpg"
          }
        },
        {
          "id": 13084,
          "store": {
            "id": 1,
            "name": "Steam",
            "slug": "steam",
            "domain": "store.steampowered.com",
            "games_count": 67190,
            "image_background": "https://media.rawg.io/media/games/c4b/c4b0cab189e73432de3a250d8cf1c84e.jpg"
          }
        },
        {
          "id": 33810,
          "store": {
            "id": 7,
            "name": "Xbox 360 Store",
            "slug": "xbox360",
            "domain": "marketplace.xbox.com",
            "games_count": 1915,
            "image_background": "https://media.rawg.io/media/games/120/1201a40e4364557b124392ee50317b99.jpg"
          }
        }
      ],
      "clip": null,
      "tags": [
        {
          "id": 31,
          "name": "Singleplayer",
          "slug": "singleplayer",
          "language": "eng",
          "games_count": 183363,
          "image_background": "https://media.rawg.io/media/games/588/588c6bdff3d4baf66ec36b1c05b793bf.jpg"
        },
        {
          "id": 40847,
          "name": "Steam Achievements",
          "slug": "steam-achievements",
          "language": "eng",
          "games_count": 27271,
          "image_background": "https://media.rawg.io/media/games/b45/b45575f34285f2c4479c9a5f719d972e.jpg"
        },
        {
          "id": 13,
          "name": "Atmospheric",
          "slug": "atmospheric",
          "language": "eng",
          "games_count": 24853,
          "image_background": "https://media.rawg.io/media/games/b7d/b7d3f1715fa8381a4e780173a197a615.jpg"
        },
        {
          "id": 40836,
          "name": "Full controller support",
          "slug": "full-controller-support",
          "language": "eng",
          "games_count": 12760,
          "image_background": "https://media.rawg.io/media/games/b49/b4912b5dbfc7ed8927b65f05b8507f6c.jpg"
        },
        {
          "id": 40849,
          "name": "Steam Cloud",
          "slug": "steam-cloud",
          "language": "eng",
          "games_count": 12450,
          "image_background": "https://media.rawg.io/media/games/310/3106b0e012271c5ffb16497b070be739.jpg"
        },
        {
          "id": 7808,
          "name": "steam-trading-cards",
          "slug": "steam-trading-cards",
          "language": "eng",
          "games_count": 7582,
          "image_background": "https://media.rawg.io/media/games/46d/46d98e6910fbc0706e2948a7cc9b10c5.jpg"
        },
        {
          "id": 42,
          "name": "Great Soundtrack",
          "slug": "great-soundtrack",
          "language": "eng",
          "games_count": 3212,
          "image_background": "https://media.rawg.io/media/games/c4b/c4b0cab189e73432de3a250d8cf1c84e.jpg"
        },
        {
          "id": 24,
          "name": "RPG",
          "slug": "rpg",
          "language": "eng",
          "games_count": 15378,
          "image_background": "https://media.rawg.io/media/games/c6b/c6bfece1daf8d06bc0a60632ac78e5bf.jpg"
        },
        {
          "id": 118,
          "name": "Story Rich",
          "slug": "story-rich",
          "language": "eng",
          "games_count": 16047,
          "image_background": "https://media.rawg.io/media/games/960/960b601d9541cec776c5fa42a00bf6c4.jpg"
        },
        {
          "id": 8,
          "name": "First-Person",
          "slug": "first-person",
          "language": "eng",
          "games_count": 24488,
          "image_background": "https://media.rawg.io/media/games/511/5118aff5091cb3efec399c808f8c598f.jpg"
        },
        {
          "id": 32,
          "name": "Sci-fi",
          "slug": "sci-fi",
          "language": "eng",
          "games_count": 15625,
          "image_background": "https://media.rawg.io/media/games/b72/b7233d5d5b1e75e86bb860ccc7aeca85.jpg"
        },
        {
          "id": 30,
          "name": "FPS",
          "slug": "fps",
          "language": "eng",
          "games_count": 11305,
          "image_background": "https://media.rawg.io/media/games/fc1/fc1307a2774506b5bd65d7e8424664a7.jpg"
        },
        {
          "id": 64,
          "name": "Fantasy",
          "slug": "fantasy",
          "language": "eng",
          "games_count": 21547,
          "image_background": "https://media.rawg.io/media/games/4e6/4e6e8e7f50c237d76f38f3c885dae3d2.jpg"
        },
        {
          "id": 26,
          "name": "Gore",
          "slug": "gore",
          "language": "eng",
          "games_count": 4653,
          "image_background": "https://media.rawg.io/media/games/c80/c80bcf321da44d69b18a06c04d942662.jpg"
        },
        {
          "id": 115,
          "name": "Controller",
          "slug": "controller",
          "language": "eng",
          "games_count": 8118,
          "image_background": "https://media.rawg.io/media/games/275/2759da6fcaa8f81f21800926168c85f6.jpg"
        },
        {
          "id": 119,
          "name": "Dystopian",
          "slug": "dystopian",
          "language": "eng",
          "games_count": 1589,
          "image_background": "https://media.rawg.io/media/games/c80/c80bcf321da44d69b18a06c04d942662.jpg"
        },
        {
          "id": 154,
          "name": "Steampunk",
          "slug": "steampunk",
          "language": "eng",
          "games_count": 1000,
          "image_background": "https://media.rawg.io/media/games/9f1/9f189c639f70f91166df415811a8b525.jpg"
        },
        {
          "id": 305,
          "name": "Linear",
          "slug": "linear",
          "language": "eng",
          "games_count": 2827,
          "image_background": "https://media.rawg.io/media/games/120/1201a40e4364557b124392ee50317b99.jpg"
        },
        {
          "id": 208,
          "name": "Alternate History",
          "slug": "alternate-history",
          "language": "eng",
          "games_count": 1329,
          "image_background": "https://media.rawg.io/media/screenshots/b01/b01237b19d38c2ed20f747a23cd7ba48.jpg"
        },
        {
          "id": 317,
          "name": "Time Travel",
          "slug": "time-travel",
          "language": "eng",
          "games_count": 1524,
          "image_background": "https://media.rawg.io/media/games/c6f/c6fc62de480c614fed2b88df0155cdcd.jpg"
        },
        {
          "id": 287,
          "name": "Political",
          "slug": "political",
          "language": "eng",
          "games_count": 458,
          "image_background": "https://media.rawg.io/media/screenshots/f93/f93ee651619bb5b273f1b51528ee872a.jpg"
        }
      ],
      "esrb_rating": {
        "id": 4,
        "name": "Mature",
        "slug": "mature"
      },
      "short_screenshots": [
        {
          "id": -1,
          "image": "https://media.rawg.io/media/games/fc1/fc1307a2774506b5bd65d7e8424664a7.jpg"
        },
        {
          "id": 98549,
          "image": "https://media.rawg.io/media/screenshots/bf0/bf07e2c6d2c888d372917d9ef453c8a4.jpg"
        },
        {
          "id": 98550,
          "image": "https://media.rawg.io/media/screenshots/9d3/9d38833952812ad7888a6dc21699934f.jpg"
        },
        {
          "id": 98551,
          "image": "https://media.rawg.io/media/screenshots/595/59572d257b6797986e4eabcd1ee023fd.jpg"
        },
        {
          "id": 98552,
          "image": "https://media.rawg.io/media/screenshots/f71/f71c23eb76f050d6180490e82d58d799.jpg"
        },
        {
          "id": 98553,
          "image": "https://media.rawg.io/media/screenshots/871/8713411d5332ceb2b4092073a6f5f3f2.jpg"
        },
        {
          "id": 98554,
          "image": "https://media.rawg.io/media/screenshots/985/985b56daa78e0a23133518d4226e9f97.jpg"
        }
      ]
    },
    {
      "id": 3439,
      "slug": "life-is-strange-episode-1-2",
      "name": "Life is Strange",
      "released": "2015-01-29",
      "tba": false,
      "background_image": "https://media.rawg.io/media/games/562/562553814dd54e001a541e4ee83a591c.jpg",
      "rating": 4.11,
      "rating_top": 5,
      "ratings": [
        {
          "id": 5,
          "title": "exceptional",
          "count": 1443,
          "percent": 43.93
        },
        {
          "id": 4,
          "title": "recommended",
          "count": 1190,
          "percent": 36.23
        },
        {
          "id": 3,
          "title": "meh",
          "count": 441,
          "percent": 13.42
        },
        {
          "id": 1,
          "title": "skip",
          "count": 211,
          "percent": 6.42
        }
      ],
      "ratings_count": 3250,
      "reviews_text_count": 22,
      "added": 13003,
      "added_by_status": {
        "yet": 698,
        "owned": 8372,
        "beaten": 2959,
        "toplay": 297,
        "dropped": 537,
        "playing": 140
      },
      "metacritic": 83,
      "playtime": 7,
      "suggestions_count": 522,
      "updated": "2022-09-28T06:09:36",
      "user_game": null,
      "reviews_count": 3285,
      "saturated_color": "0f0f0f",
      "dominant_color": "0f0f0f",
      "platforms": [
        {
          "platform": {
            "id": 4,
            "name": "PC",
            "slug": "pc",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 483690,
            "image_background": "https://media.rawg.io/media/games/618/618c2031a07bbff6b4f611f10b6bcdbc.jpg"
          },
          "released_at": "2015-01-29",
          "requirements_en": null,
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 6,
            "name": "Linux",
            "slug": "linux",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 72202,
            "image_background": "https://media.rawg.io/media/games/9dd/9ddabb34840ea9227556670606cf8ea3.jpg"
          },
          "released_at": "2015-01-29",
          "requirements_en": null,
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 16,
            "name": "PlayStation 3",
            "slug": "playstation3",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 3582,
            "image_background": "https://media.rawg.io/media/games/562/562553814dd54e001a541e4ee83a591c.jpg"
          },
          "released_at": "2015-01-29",
          "requirements_en": null,
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 5,
            "name": "macOS",
            "slug": "macos",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 98332,
            "image_background": "https://media.rawg.io/media/games/b54/b54598d1d5cc31899f4f0a7e3122a7b0.jpg"
          },
          "released_at": "2015-01-29",
          "requirements_en": null,
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 3,
            "name": "iOS",
            "slug": "ios",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 75474,
            "image_background": "https://media.rawg.io/media/games/be0/be084b850302abe81675bc4ffc08a0d0.jpg"
          },
          "released_at": "2015-01-29",
          "requirements_en": {
            "minimum": "iPhone 5s, iPad Air, iPad Air Cellular, iPad Mini Retina, iPad Mini Retina Cellular, iPhone 6, iPhone 6 Plus, iPad Air 2, iPad Air 2 Cellular, iPad Mini 3, iPad Mini 3 Cellular, iPod Touch Sixth Gen, iPhone 6s, iPhone 6s Plus, iPad Mini 4, iPad Mini 4 Cellular, iPad Pro, iPad Pro Cellular, iPad Pro 9.7, iPad Pro 9.7 Cellular, iPhone SE, iPhone 7, iPhone 7 Plus, iPad 6 1 1, iPad 6 1 2, iPad 7 1, iPad 7 2, iPad 7 3, iPad 7 4, iPhone 8, iPhone 8 Plus, iPhone X, iPad 7 5, iPad 7 6, iPhone X S, iPhone X S Max, iPhone X R, iPad 8 1 2, iPad 8 3 4, iPad 8 5 6, iPad 8 7 8, iPad Mini 5, iPad Mini 5 Cellular, iPad Air 3, iPad Air 3 Cellular, iPod Touch Seventh Gen"
          },
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 14,
            "name": "Xbox 360",
            "slug": "xbox360",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 2777,
            "image_background": "https://media.rawg.io/media/games/15c/15c95a4915f88a3e89c821526afe05fc.jpg"
          },
          "released_at": "2015-01-29",
          "requirements_en": null,
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 21,
            "name": "Android",
            "slug": "android",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 50878,
            "image_background": "https://media.rawg.io/media/games/c7a/c7a71a0531a9518236d99d0d60abe447.jpg"
          },
          "released_at": "2015-01-29",
          "requirements_en": {
            "minimum": "6.0 and up"
          },
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 18,
            "name": "PlayStation 4",
            "slug": "playstation4",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 6479,
            "image_background": "https://media.rawg.io/media/games/fc1/fc1307a2774506b5bd65d7e8424664a7.jpg"
          },
          "released_at": "2015-01-29",
          "requirements_en": null,
          "requirements_ru": null
        },
        {
          "platform": {
            "id": 1,
            "name": "Xbox One",
            "slug": "xbox-one",
            "image": null,
            "year_end": null,
            "year_start": null,
            "games_count": 5399,
            "image_background": "https://media.rawg.io/media/games/490/49016e06ae2103881ff6373248843069.jpg"
          },
          "released_at": "2015-01-29",
          "requirements_en": null,
          "requirements_ru": null
        }
      ],
      "parent_platforms": [
        {
          "platform": {
            "id": 1,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 2,
            "name": "PlayStation",
            "slug": "playstation"
          }
        },
        {
          "platform": {
            "id": 3,
            "name": "Xbox",
            "slug": "xbox"
          }
        },
        {
          "platform": {
            "id": 4,
            "name": "iOS",
            "slug": "ios"
          }
        },
        {
          "platform": {
            "id": 8,
            "name": "Android",
            "slug": "android"
          }
        },
        {
          "platform": {
            "id": 5,
            "name": "Apple Macintosh",
            "slug": "mac"
          }
        },
        {
          "platform": {
            "id": 6,
            "name": "Linux",
            "slug": "linux"
          }
        }
      ],
      "genres": [
        {
          "id": 3,
          "name": "Adventure",
          "slug": "adventure",
          "games_count": 123284,
          "image_background": "https://media.rawg.io/media/games/b54/b54598d1d5cc31899f4f0a7e3122a7b0.jpg"
        }
      ],
      "stores": [
        {
          "id": 451321,
          "store": {
            "id": 5,
            "name": "GOG",
            "slug": "gog",
            "domain": "gog.com",
            "games_count": 4412,
            "image_background": "https://media.rawg.io/media/games/562/562553814dd54e001a541e4ee83a591c.jpg"
          }
        },
        {
          "id": 3702,
          "store": {
            "id": 3,
            "name": "PlayStation Store",
            "slug": "playstation-store",
            "domain": "store.playstation.com",
            "games_count": 7797,
            "image_background": "https://media.rawg.io/media/games/f87/f87457e8347484033cb34cde6101d08d.jpg"
          }
        },
        {
          "id": 35603,
          "store": {
            "id": 1,
            "name": "Steam",
            "slug": "steam",
            "domain": "store.steampowered.com",
            "games_count": 67190,
            "image_background": "https://media.rawg.io/media/games/c4b/c4b0cab189e73432de3a250d8cf1c84e.jpg"
          }
        },
        {
          "id": 35602,
          "store": {
            "id": 2,
            "name": "Xbox Store",
            "slug": "xbox-store",
            "domain": "microsoft.com",
            "games_count": 4752,
            "image_background": "https://media.rawg.io/media/games/4cf/4cfc6b7f1850590a4634b08bfab308ab.jpg"
          }
        },
        {
          "id": 217695,
          "store": {
            "id": 8,
            "name": "Google Play",
            "slug": "google-play",
            "domain": "play.google.com",
            "games_count": 16868,
            "image_background": "https://media.rawg.io/media/games/8d6/8d69eb6c32ed6acfd75f82d532144993.jpg"
          }
        },
        {
          "id": 245823,
          "store": {
            "id": 7,
            "name": "Xbox 360 Store",
            "slug": "xbox360",
            "domain": "marketplace.xbox.com",
            "games_count": 1915,
            "image_background": "https://media.rawg.io/media/games/120/1201a40e4364557b124392ee50317b99.jpg"
          }
        },
        {
          "id": 44714,
          "store": {
            "id": 4,
            "name": "App Store",
            "slug": "apple-appstore",
            "domain": "apps.apple.com",
            "games_count": 73767,
            "image_background": "https://media.rawg.io/media/games/713/713269608dc8f2f40f5a670a14b2de94.jpg"
          }
        }
      ],
      "clip": null,
      "tags": [
        {
          "id": 31,
          "name": "Singleplayer",
          "slug": "singleplayer",
          "language": "eng",
          "games_count": 183363,
          "image_background": "https://media.rawg.io/media/games/588/588c6bdff3d4baf66ec36b1c05b793bf.jpg"
        },
        {
          "id": 13,
          "name": "Atmospheric",
          "slug": "atmospheric",
          "language": "eng",
          "games_count": 24853,
          "image_background": "https://media.rawg.io/media/games/b7d/b7d3f1715fa8381a4e780173a197a615.jpg"
        },
        {
          "id": 40836,
          "name": "Full controller support",
          "slug": "full-controller-support",
          "language": "eng",
          "games_count": 12760,
          "image_background": "https://media.rawg.io/media/games/b49/b4912b5dbfc7ed8927b65f05b8507f6c.jpg"
        },
        {
          "id": 7808,
          "name": "steam-trading-cards",
          "slug": "steam-trading-cards",
          "language": "eng",
          "games_count": 7582,
          "image_background": "https://media.rawg.io/media/games/46d/46d98e6910fbc0706e2948a7cc9b10c5.jpg"
        },
        {
          "id": 42,
          "name": "Great Soundtrack",
          "slug": "great-soundtrack",
          "language": "eng",
          "games_count": 3212,
          "image_background": "https://media.rawg.io/media/games/c4b/c4b0cab189e73432de3a250d8cf1c84e.jpg"
        },
        {
          "id": 118,
          "name": "Story Rich",
          "slug": "story-rich",
          "language": "eng",
          "games_count": 16047,
          "image_background": "https://media.rawg.io/media/games/960/960b601d9541cec776c5fa42a00bf6c4.jpg"
        },
        {
          "id": 149,
          "name": "Third Person",
          "slug": "third-person",
          "language": "eng",
          "games_count": 7938,
          "image_background": "https://media.rawg.io/media/games/5c0/5c0dd63002cb23f804aab327d40ef119.jpg"
        },
        {
          "id": 189,
          "name": "Female Protagonist",
          "slug": "female-protagonist",
          "language": "eng",
          "games_count": 9211,
          "image_background": "https://media.rawg.io/media/games/62c/62c7c8b28a27b83680b22fb9d33fc619.jpg"
        },
        {
          "id": 41,
          "name": "Dark",
          "slug": "dark",
          "language": "eng",
          "games_count": 12166,
          "image_background": "https://media.rawg.io/media/games/d5a/d5a24f9f71315427fa6e966fdd98dfa6.jpg"
        },
        {
          "id": 141,
          "name": "Point & Click",
          "slug": "point-click",
          "language": "eng",
          "games_count": 10511,
          "image_background": "https://media.rawg.io/media/games/df2/df20fd77db56ae7b0a26a7ff4baa9ccc.jpg"
        },
        {
          "id": 111,
          "name": "Short",
          "slug": "short",
          "language": "eng",
          "games_count": 50413,
          "image_background": "https://media.rawg.io/media/games/7ac/7aca7ccf0e70cd0974cb899ab9e5158e.jpg"
        },
        {
          "id": 117,
          "name": "Mystery",
          "slug": "mystery",
          "language": "eng",
          "games_count": 10508,
          "image_background": "https://media.rawg.io/media/games/be0/be084b850302abe81675bc4ffc08a0d0.jpg"
        },
        {
          "id": 145,
          "name": "Choices Matter",
          "slug": "choices-matter",
          "language": "eng",
          "games_count": 2628,
          "image_background": "https://media.rawg.io/media/games/6cd/6cd653e0aaef5ff8bbd295bf4bcb12eb.jpg"
        },
        {
          "id": 120,
          "name": "Memes",
          "slug": "memes",
          "language": "eng",
          "games_count": 1461,
          "image_background": "https://media.rawg.io/media/games/214/2140885d34e3a3398b45036e5d870971.jpg"
        },
        {
          "id": 91,
          "name": "Walking Simulator",
          "slug": "walking-simulator",
          "language": "eng",
          "games_count": 5700,
          "image_background": "https://media.rawg.io/media/games/b6b/b6b20bfc4b34e312dbc8aac53c95a348.jpg"
        },
        {
          "id": 406,
          "name": "Story",
          "slug": "story",
          "language": "eng",
          "games_count": 10791,
          "image_background": "https://media.rawg.io/media/games/74b/74b239f6ef0216a2f66e652d54abb2e6.jpg"
        },
        {
          "id": 218,
          "name": "Multiple Endings",
          "slug": "multiple-endings",
          "language": "eng",
          "games_count": 5891,
          "image_background": "https://media.rawg.io/media/games/618/618c2031a07bbff6b4f611f10b6bcdbc.jpg"
        },
        {
          "id": 232,
          "name": "Episodic",
          "slug": "episodic",
          "language": "eng",
          "games_count": 422,
          "image_background": "https://media.rawg.io/media/games/883/883bc3050f9a4115d1968ece56bddfc2.jpg"
        },
        {
          "id": 317,
          "name": "Time Travel",
          "slug": "time-travel",
          "language": "eng",
          "games_count": 1524,
          "image_background": "https://media.rawg.io/media/games/c6f/c6fc62de480c614fed2b88df0155cdcd.jpg"
        },
        {
          "id": 295,
          "name": "Soundtrack",
          "slug": "soundtrack",
          "language": "eng",
          "games_count": 2603,
          "image_background": "https://media.rawg.io/media/screenshots/5ce/5ce17393fdcd1a4356187e388fa21723.jpeg"
        },
        {
          "id": 302,
          "name": "Time Manipulation",
          "slug": "time-manipulation",
          "language": "eng",
          "games_count": 324,
          "image_background": "https://media.rawg.io/media/games/3fb/3fbe3c1d67fdda2685b0f49e06813038.jpg"
        },
        {
          "id": 992,
          "name": "student",
          "slug": "student",
          "language": "eng",
          "games_count": 1414,
          "image_background": "https://media.rawg.io/media/screenshots/14e/14e8044e7e97fc9d23fcf53c2b78b9a1.jpg"
        },
        {
          "id": 2682,
          "name": "strange",
          "slug": "strange",
          "language": "eng",
          "games_count": 335,
          "image_background": "https://media.rawg.io/media/screenshots/ec9/ec9efc5bf03883161a1bc40cdb83e64a.jpg"
        },
        {
          "id": 3197,
          "name": "photography",
          "slug": "photography",
          "language": "eng",
          "games_count": 190,
          "image_background": "https://media.rawg.io/media/screenshots/d9c/d9cf210bde7895579ca53a9e58d20182.jpg"
        }
      ],
      "esrb_rating": {
        "id": 4,
        "name": "Mature",
        "slug": "mature"
      },
      "short_screenshots": [
        {
          "id": -1,
          "image": "https://media.rawg.io/media/games/562/562553814dd54e001a541e4ee83a591c.jpg"
        },
        {
          "id": 1826487,
          "image": "https://media.rawg.io/media/screenshots/edf/edfcbdf85f02f871263dabf1b4f0aa87.jpg"
        },
        {
          "id": 1826488,
          "image": "https://media.rawg.io/media/screenshots/4c6/4c6da2f36396d4ed51f82ba6159fa39b.jpg"
        },
        {
          "id": 1826489,
          "image": "https://media.rawg.io/media/screenshots/6aa/6aa56ef1485c8b287a913fa842883daa.jpg"
        },
        {
          "id": 1826490,
          "image": "https://media.rawg.io/media/screenshots/cb1/cb148b52fe857f5b0b83ae9c01f56d8e.jpg"
        },
        {
          "id": 1826491,
          "image": "https://media.rawg.io/media/screenshots/aea/aea38b33b90054f8fe4cc8bb05253b1d.jpg"
        },
        {
          "id": 1826492,
          "image": "https://media.rawg.io/media/screenshots/c1d/c1d6333b2da0f920e8de10c14d3c6093.jpg"
        }
      ]
    }
  ],
  "seo_title": "All Games",
  "seo_description": "",
  "seo_keywords": "",
  "seo_h1": "All Games",
  "noindex": false,
  "nofollow": false,
  "description": "",
  "filters": {
    "years": [
      {
        "from": 2020,
        "to": 2022,
        "filter": "2020-01-01,2022-12-31",
        "decade": 2020,
        "years": [
          {
            "year": 2022,
            "count": 132651,
            "nofollow": false
          },
          {
            "year": 2021,
            "count": 173156,
            "nofollow": false
          },
          {
            "year": 2020,
            "count": 132981,
            "nofollow": false
          }
        ],
        "nofollow": true,
        "count": 438788
      },
      {
        "from": 2010,
        "to": 2019,
        "filter": "2010-01-01,2019-12-31",
        "decade": 2010,
        "years": [
          {
            "year": 2019,
            "count": 79708,
            "nofollow": false
          },
          {
            "year": 2018,
            "count": 71535,
            "nofollow": false
          },
          {
            "year": 2017,
            "count": 56532,
            "nofollow": true
          },
          {
            "year": 2016,
            "count": 41359,
            "nofollow": true
          },
          {
            "year": 2015,
            "count": 26447,
            "nofollow": true
          },
          {
            "year": 2014,
            "count": 15618,
            "nofollow": true
          },
          {
            "year": 2013,
            "count": 6342,
            "nofollow": true
          },
          {
            "year": 2012,
            "count": 5377,
            "nofollow": true
          },
          {
            "year": 2011,
            "count": 4312,
            "nofollow": true
          },
          {
            "year": 2010,
            "count": 3891,
            "nofollow": true
          }
        ],
        "nofollow": true,
        "count": 311121
      },
      {
        "from": 2000,
        "to": 2009,
        "filter": "2000-01-01,2009-12-31",
        "decade": 2000,
        "years": [
          {
            "year": 2009,
            "count": 3104,
            "nofollow": true
          },
          {
            "year": 2008,
            "count": 2023,
            "nofollow": true
          },
          {
            "year": 2007,
            "count": 1548,
            "nofollow": true
          },
          {
            "year": 2006,
            "count": 1269,
            "nofollow": true
          },
          {
            "year": 2005,
            "count": 1136,
            "nofollow": true
          },
          {
            "year": 2004,
            "count": 1140,
            "nofollow": true
          },
          {
            "year": 2003,
            "count": 1129,
            "nofollow": true
          },
          {
            "year": 2002,
            "count": 979,
            "nofollow": true
          },
          {
            "year": 2001,
            "count": 1094,
            "nofollow": true
          },
          {
            "year": 2000,
            "count": 984,
            "nofollow": true
          }
        ],
        "nofollow": true,
        "count": 14406
      },
      {
        "from": 1990,
        "to": 1999,
        "filter": "1990-01-01,1999-12-31",
        "decade": 1990,
        "years": [
          {
            "year": 1999,
            "count": 777,
            "nofollow": true
          },
          {
            "year": 1998,
            "count": 716,
            "nofollow": true
          },
          {
            "year": 1997,
            "count": 860,
            "nofollow": true
          },
          {
            "year": 1996,
            "count": 748,
            "nofollow": true
          },
          {
            "year": 1995,
            "count": 855,
            "nofollow": true
          },
          {
            "year": 1994,
            "count": 810,
            "nofollow": true
          },
          {
            "year": 1993,
            "count": 738,
            "nofollow": true
          },
          {
            "year": 1992,
            "count": 640,
            "nofollow": true
          },
          {
            "year": 1991,
            "count": 576,
            "nofollow": true
          },
          {
            "year": 1990,
            "count": 527,
            "nofollow": true
          }
        ],
        "nofollow": true,
        "count": 7247
      },
      {
        "from": 1980,
        "to": 1989,
        "filter": "1980-01-01,1989-12-31",
        "decade": 1980,
        "years": [
          {
            "year": 1989,
            "count": 425,
            "nofollow": true
          },
          {
            "year": 1988,
            "count": 313,
            "nofollow": true
          },
          {
            "year": 1987,
            "count": 338,
            "nofollow": true
          },
          {
            "year": 1986,
            "count": 245,
            "nofollow": true
          },
          {
            "year": 1985,
            "count": 229,
            "nofollow": true
          },
          {
            "year": 1984,
            "count": 185,
            "nofollow": true
          },
          {
            "year": 1983,
            "count": 206,
            "nofollow": true
          },
          {
            "year": 1982,
            "count": 148,
            "nofollow": true
          },
          {
            "year": 1981,
            "count": 65,
            "nofollow": true
          },
          {
            "year": 1980,
            "count": 35,
            "nofollow": true
          }
        ],
        "nofollow": true,
        "count": 2189
      },
      {
        "from": 1970,
        "to": 1979,
        "filter": "1970-01-01,1979-12-31",
        "decade": 1970,
        "years": [
          {
            "year": 1979,
            "count": 15,
            "nofollow": true
          },
          {
            "year": 1978,
            "count": 17,
            "nofollow": true
          },
          {
            "year": 1977,
            "count": 13,
            "nofollow": true
          },
          {
            "year": 1976,
            "count": 7,
            "nofollow": true
          },
          {
            "year": 1975,
            "count": 3,
            "nofollow": true
          },
          {
            "year": 1974,
            "count": 2,
            "nofollow": true
          },
          {
            "year": 1973,
            "count": 1,
            "nofollow": true
          },
          {
            "year": 1972,
            "count": 2,
            "nofollow": true
          },
          {
            "year": 1971,
            "count": 5,
            "nofollow": true
          },
          {
            "year": 1970,
            "count": 1,
            "nofollow": true
          }
        ],
        "nofollow": true,
        "count": 66
      },
      {
        "from": 1960,
        "to": 1969,
        "filter": "1960-01-01,1969-12-31",
        "decade": 1960,
        "years": [
          {
            "year": 1962,
            "count": 1,
            "nofollow": true
          }
        ],
        "nofollow": true,
        "count": 1
      },
      {
        "from": 1950,
        "to": 1959,
        "filter": "1950-01-01,1959-12-31",
        "decade": 1950,
        "years": [
          {
            "year": 1957,
            "count": 1,
            "nofollow": true
          }
        ],
        "nofollow": true,
        "count": 1
      }
    ]
  },
  "nofollow_collections": [
    "stores"
  ]
}
export default testApi;