import React from 'react'
import useGamesApi from '../../hooks/gamesApi.service'
import { useEffect } from 'react'

function Games() {
    const gamesApi = useGamesApi();
       
    useEffect(() => {
        const uploadGames = async () => {
            const newsGames = await gamesApi.testFetchGames();
            console.log('test', newsGames);
        }
        uploadGames();
    },[gamesApi]);

  return (
    <div>games</div>
  )
}

export default Games